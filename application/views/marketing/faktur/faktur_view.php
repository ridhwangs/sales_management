   <div class="row">
       <div class="container-fluid print-section">
           <div class="col-md-12">
               <div class="tile">
                   <div class="tile-body readonly">
                       <section class="invoice ">
                           <div class="row mb-4">
                               <div class="col-6">
                                   <h2 class="page-header"><i class="fa fa-globe"></i> Proses Kendaraan</h2>
                               </div>
                           </div>
                           <div class="row invoice-info">
                               <div class="col-4">Marketing :
                                   <address><strong><?= $data->nm_marketing; ?>.</strong><br><?= $data->alamat_marketing; ?><br>Phone: <?= $data->no_marketing; ?><br>Email: <?= $data->email_marketing; ?></address>
                               </div>
                               <div class="col-4">Konsumen :
                                   <address><strong><?= $data->nm_konsumen; ?></strong><br><?= $data->alamat_konsumen; ?><br>Phone: <?= $data->no_konsumen; ?><br>Email: <?= $data->email_konsumen; ?></address>
                               </div>
                               <div class="col-4">
                                   <b>Invoice #<?= str_pad($this->uri->segment(4), 6, '0', STR_PAD_LEFT); ?></b><br><br>
                                   Tgl Faktur:</b> <?= date('d/m/Y', strtotime($data->tgl_faktur)) ?><br>
                                   <b>Tgl Estimasi Selesai:</b> <?= date('d/m/Y', strtotime($data->tgl_estimasi_selesai)) ?><br>
                                   <?php if (!empty($data->tgl_selesai)) : ?>
                                       <b>Tgl Selesai:</b> <?= date('d/m/Y', strtotime($data->tgl_selesai)) ?>
                                   <?php endif; ?>
                               </div>
                           </div>
                           <div class="row">
                               <div class="col-12 table-responsive">
                                   <table class="table table-striped">
                                       <thead>
                                           <tr>
                                               <th>No. SPK</th>
                                               <th>Merk</th>
                                               <th>Tipe</th>
                                               <th>Warna</th>
                                               <th>No Rangka</th>
                                               <th>Karoseri</th>
                                               <th>Tipe Rear Body</th>
                                               <th>Harga Dasar (OTR)</th>
                                               <th>Diskon</th>
                                               <th>Harga Estimasi</th>
                                           </tr>
                                       </thead>
                                       <tbody>
                                           <tr>
                                               <td><?= $data->no_spk; ?></td>
                                               <td><?= $data->merk; ?></td>
                                               <td><?= $data->tipe; ?></td>
                                               <td><?= $data->warna; ?></td>

                                               <td><?= $data->no_rangka; ?></td>
                                               <td><?= $data->nm_karoseri; ?></td>
                                               <td><?= $data->tipe_rear_body; ?></td>
                                               <td align="right">Rp. <?= number_format($data->harga_dasar); ?></td>
                                               <td align="right">Rp. <?= number_format($data->diskon); ?></td>
                                               <td align="right">Rp. <?= number_format($data->harga_estimasi); ?></td>
                                           </tr>
                                       </tbody>
                                   </table>
                               </div>
                           </div>
                           <div class="row d-print-none mt-2">
                               <div class="col-12 text-right text-white"><a class="btn btn-primary" onclick="printDiv('.print-section');" id="print"><i class="fa fa-print"></i> Print</a></div>
                           </div>
                       </section>
                       <div class="display" id="break_page" style='page-break-after:always'></div>
                   </div>
               </div>
           </div>
       </div>
   </div>

   <script>
       function printDiv(elem) {
           renderMe($('<div/>').append($(elem).clone()).html());
       }

       function renderMe(data) {
           $('#cover-spin').show(0);
           var mywindow = window.open('', 'print-section', 'height=' + screen.height + ',width=' + screen.width);
           popupBlockerChecker.check(mywindow);
           mywindow.document.write('<html><head><title></title>');
           mywindow.document.write('<link rel="stylesheet" href="<?= base_url(); ?>node_modules/bootstrap/dist/css/bootstrap.css" crossorigin="anonymous">');
           mywindow.document.write('<link rel="stylesheet" type="text/css" href="<?= assets_url(); ?>css/main.css">');
           mywindow.document.write('</head><body >');
           mywindow.document.write(data);
           mywindow.document.write('</body></html>');
           setTimeout(function() {
               mywindow.print();
               mywindow.close();
               $('#cover-spin').hide(0);
           }, 1000)
           return true;
       }
   </script>