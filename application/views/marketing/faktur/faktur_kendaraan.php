   <div class="row">
       <div class="container">
           <div class="col-md-12">
               <div class="tile">
                   <div class="tile-title-w-btn">
                       <h5>Step-1 : Form Kendaraan<h5>
                   </div>
                   <form method="POST" id="form-kendaraan" action="<?= site_url('marketing/faktur/create/kendaraan'); ?>" autocomplete="off">
                       <input type="hidden" value="<?= $this->uri->segment('4'); ?>" name="id_spk">
                       <div class="tile-body">

                           <div class="form-group row">
                               <label class="control-label col-md-3 float-right small" for="merk">Merk</label>
                               <div class="col-md-9 input-group">
                                   <input type="text" name="merk" class="form-control form-control-sm" id="merk" placeholder="Merk" autofocus>
                               </div>
                           </div>
                           <div class="form-group row">
                               <label class="control-label col-md-3 float-right small" for="tipe">Tipe</label>
                               <div class="col-md-9 input-group">
                                   <input type="text" name="tipe" class="form-control form-control-sm" id="tipe" placeholder="Tipe">
                               </div>
                           </div>
                           <div class="form-group row">
                               <label class="control-label col-md-3 float-right small" for="warna">Warna</label>
                               <div class="col-md-9 input-group">
                                   <input type="text" name="warna" class="form-control form-control-sm" id="warna" placeholder="Warna">
                               </div>
                           </div>
                           <div class="form-group row">
                               <label class="control-label col-md-3 float-right small" for="harga_dasar">Harga Dasar (OTR)</label>
                               <div class="col-md-9 input-group">
                                   <input type="text" name="harga_dasar" class="form-control form-control-sm rupiah" id="harga_dasar" placeholder="Harga Dasar (Rp.)" required>
                               </div>
                           </div>
                           <div class="form-group row">
                               <label class="control-label col-md-3 float-right small" for="diskon">Diskon</label>
                               <div class="col-md-9 input-group">
                                   <input type="text" name="diskon" class="form-control form-control-sm rupiah" id="diskon" placeholder="Diskon (Rp.)" required>
                               </div>
                           </div>
                           <div class="form-group row">
                               <label class="control-label col-md-3 float-right small" for="harga_estimasi">Harga Estimasi</label>
                               <div class="col-md-9 input-group">
                                   <input type="text" name="harga_estimasi" class="form-control form-control-sm rupiah" id="harga_estimasi" placeholder="Harga Estimasi (Rp.)" required>
                               </div>
                           </div>
                           <div class="form-group row">
                               <label class="control-label col-md-3 float-right small" for="no_rangka">No Rangka</label>
                               <div class="col-md-9 input-group">
                                   <input type="text" name="no_rangka" class="form-control form-control-sm" id="no_rangka" placeholder="No Rangka" required>
                               </div>
                           </div>
                       </div>

                   </form>
                   <div class="tile-footer">
                       <button class="btn btn-primary btn-block" id="btn-simpan" form="form-kendaraan">Selanjutnya >></button>
                   </div>
               </div>
           </div>
       </div>
   </div>