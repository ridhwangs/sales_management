   <div class="row">
       <div class="container-fluid ">
           <div class="col-md-12">
               <div class="tile">
                   <div class="tile-title-w-btn">
                       <h5>Step-1 : Form Kendaraan<h5>
                   </div>
                   <input type="hidden" value="<?= $this->uri->segment('4'); ?>" name="id_spk">
                   <div class="tile-body readonly">

                       <div class="form-group row">
                           <label class="control-label col-md-3 float-right small" for="merk">Merk</label>
                           <div class="col-md-9 input-group">
                               <input type="text" name="merk" class="form-control form-control-sm" id="merk" placeholder="Merk">
                           </div>
                       </div>
                       <div class="form-group row">
                           <label class="control-label col-md-3 float-right small" for="tipe">Tipe</label>
                           <div class="col-md-9 input-group">
                               <input type="text" name="tipe" class="form-control form-control-sm" id="tipe" placeholder="Tipe">
                           </div>
                       </div>
                       <div class="form-group row">
                           <label class="control-label col-md-3 float-right small" for="warna">Warna</label>
                           <div class="col-md-9 input-group">
                               <input type="text" name="warna" class="form-control form-control-sm" id="warna" placeholder="Warna">
                           </div>
                       </div>
                       <div class="form-group row">
                           <label class="control-label col-md-3 float-right small" for="harga_dasar">Harga Dasar (OTR)</label>
                           <div class="col-md-9 input-group">
                               <input type="text" name="harga_dasar" class="form-control form-control-sm rupiah" id="harga_dasar" value="<?= $data_kendaraan->harga_dasar; ?>" placeholder="Harga Dasar (Rp.)" required>
                           </div>
                       </div>
                       <div class="form-group row">
                           <label class="control-label col-md-3 float-right small" for="diskon">Diskon</label>
                           <div class="col-md-9 input-group">
                               <input type="text" name="diskon" class="form-control form-control-sm rupiah" id="diskon" value="<?= $data_kendaraan->diskon; ?>" placeholder="Diskon (Rp.)" required>
                           </div>
                       </div>
                       <div class="form-group row">
                           <label class="control-label col-md-3 float-right small" for="harga_estimasi">Harga Estimasi</label>
                           <div class="col-md-9 input-group">
                               <input type="text" name="harga_estimasi" class="form-control form-control-sm rupiah" id="harga_estimasi" value="<?= $data_kendaraan->harga_estimasi; ?>" placeholder="Harga Estimasi (Rp.)" required>
                           </div>
                       </div>
                       <div class="form-group row">
                           <label class="control-label col-md-3 float-right small" for="no_rangka">No Rangka</label>
                           <div class="col-md-9 input-group">
                               <input type="text" name="no_rangka" class="form-control form-control-sm" id="no_rangka" placeholder="No Rangka" required>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <div class="container-fluid">
           <div class="col-md-12">
               <div class="tile">
                   <div class="tile-title-w-btn">
                       <h5><a href="<?= site_url('marketing/faktur/delete/karoseri/' . $id_faktur); ?>"><i class="fa fa-trash" aria-hidden="true" style="color: red;"></i></a> Step-2 : Form Karoseri<h5>
                   </div>
                   <input type="hidden" value="<?= $this->uri->segment('4'); ?>" name="id_kendaraan">
                   <div class="tile-body readonly">

                       <div class="form-group row">
                           <label class="control-label col-md-3 float-right small" for="kd_karoseri">Karoseri</label>
                           <div class="col-md-9 input-group">
                               <select name="kd_karoseri" class="form-control form-control-sm" id="kd_karoseri" required>
                                   <option value="" selected disabled>Silahkan Pilih</option>
                                   <?php
                                    foreach ($kd_karoseri as $rows) {
                                        echo '<option value="' . $rows->kd_karoseri . '">' . $rows->kd_karoseri . ' - ' . $rows->nm_karoseri . ' [' . $rows->estimasi_hari . ' Hari]</option>';
                                    }
                                    ?>
                               </select>
                           </div>
                       </div>
                       <div class="form-group row">
                           <label class="control-label col-md-3 float-right small" for="tipe_rear_body">Tipe Rear Body</label>
                           <div class="col-md-9 input-group">
                               <input type="text" name="tipe_rear_body" class="form-control form-control-sm" id="tipe_rear_body" placeholder="Tipe Rear Body">
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>

       <div class="container-fluid">
           <div class="col-md-12">
               <div class="tile">
                   <div class="tile-title-w-btn">
                       <h5>Step-3 : Form Proses Faktur<h5>
                   </div>
                   <form method="POST" id="form-proses" action="<?= site_url('marketing/faktur/create/proses'); ?>" autocomplete="off">
                       <input type="hidden" value="<?= $this->uri->segment('4'); ?>" name="id_faktur">
                       <div class="tile-body">
                           <div class="form-group row">
                               <label class="control-label col-md-3 float-right small" for="tgl_faktur">Tanggal Faktur</label>
                               <div class="col-md-9 input-group">
                                   <input type="date" name="tgl_faktur" class="form-control form-control-sm" id="tgl_faktur" value="<?= date('Y-m-d'); ?>" readonly>
                               </div>
                           </div>
                       </div>
                       <div class="tile-body">
                           <div class="form-group row">
                               <label class="control-label col-md-3 float-right small" for="tgl_estimasi_selesai">Tanggal Estimasi Selesai</label>
                               <div class="col-md-9 input-group">
                                   <input type="date" name="tgl_estimasi_selesai" class="form-control form-control-sm" value="<?= date('Y-m-d', strtotime('+3 days')) ?>" id="tgl_estimasi_selesai" readonly>
                               </div>
                           </div>
                       </div>
                       <?php
                        if ($data_faktur->kd_karoseri != "NONK") :
                        ?>
                           <div class="tile-body">
                               <div class="form-group row">
                                   <label class="control-label col-md-3 float-right small" for="tgl_estimasi_karoseri">Tanggal Estimasi Karoseri</label>
                                   <div class="col-md-9 input-group">
                                       <input type="date" name="tgl_estimasi_karoseri" class="form-control form-control-sm" id="tgl_estimasi_karoseri" value="<?= date('Y-m-d', strtotime('+' . $data_karoseri->estimasi_hari . ' days')) ?>" readonly>
                                   </div>
                               </div>
                           </div>
                       <?php endif; ?>
                       <div class="tile-body">
                           <div class="form-group row">
                               <label class="control-label col-md-3 float-right small" for="tgl_estimasi_stnk">Tanggal Estimasi STNK</label>
                               <div class="col-md-9 input-group">
                                   <input type="date" name="tgl_estimasi_stnk" class="form-control form-control-sm" id="tgl_estimasi_stnk" value="<?= date('Y-m-d', strtotime('+7 days')) ?>" readonly>
                               </div>
                           </div>
                       </div>
                   </form>
                   <div class="tile-footer">
                       <button class="btn btn-primary btn-block" onclick="proses_faktur()" id="btn-simpan" form="form-proses">Proses Faktur</button>
                   </div>
               </div>
           </div>
       </div>
   </div>

   <script>
       $(document).ready(function() {
           function proses_faktur() {
               $('#cover-spin').show(0);
           }
           $('.readonly').find('input, textarea, select').attr('disabled', 'disabled');
           $("#merk").val('<?= $data_kendaraan->merk; ?>');
           $("#tipe").val('<?= $data_kendaraan->tipe; ?>');
           $("#warna").val('<?= $data_kendaraan->warna; ?>');
           $("#no_rangka").val('<?= $data_kendaraan->no_rangka; ?>');

           $("#kd_karoseri").val('<?= $data_faktur->kd_karoseri; ?>');
           $("#tipe_rear_body").val('<?= $data_faktur->tipe_rear_body; ?>');
       });
   </script>