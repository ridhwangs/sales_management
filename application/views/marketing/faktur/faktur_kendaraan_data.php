<div class="app-title">
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-mmksi.png'); ?>" alt="logo-mmksi" width="50px">
    </div>
    <div class="text-center">
        <h1> <?= $page_header; ?></h1>
        <p>Sales Management System</p>
    </div>
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-fuso.png'); ?>" alt="logo-fuso" width="50px">
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-title-w-btn">
                    <h5>Faktur Kendaraan<h5>
                </div>
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                                <tr class="text-center">
                                    <th>No SPK</th>
                                    <th>Tanggal SPK</th>
                                    <th>Tanggal Faktur</th>
                                    <th>Nama Konsumen</th>
                                    <th>Merk</th>
                                    <th>Tipe</th>
                                    <th>Warna</th>
                                    <th>Harga Dasar</th>
                                    <th>Diskon</th>
                                    <th>Harga Estimasi</th>
                                    <th>No Rangka</th>
                                    <th>Tgl Estimai Selesai</th>
                                    <th>Tgl Selesai</th>
                                    <th width="1px">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Fetch data dari database-->
                                <?php
                                foreach ($data->result() as $row) :
                                    $dt_konsumen = $this->crud_model->read('dt_konsumen', ['id_konsumen' => $row->id_konsumen])->row();
                                ?>
                                    <tr class="uppercase">
                                        <td><?php echo $row->no_spk; ?></td>
                                        <td><?php echo $row->tanggal; ?></td>
                                        <td><?php echo $row->tgl_faktur; ?></td>
                                        <td>[<b><?php echo $dt_konsumen->jenis; ?></b>] <?php echo $dt_konsumen->nm_lengkap; ?></td>
                                        <td><?= $row->merk; ?></td>
                                        <td><?= $row->tipe; ?></td>
                                        <td><?= $row->warna; ?></td>
                                        <td>
                                            <div class="text-right"><?= number_format($row->harga_dasar); ?></div>
                                        </td>
                                        <td>
                                            <div class="text-right"><?= number_format($row->diskon); ?></div>
                                        </td>
                                        <td>
                                            <div class="text-right"><?= number_format($row->harga_estimasi); ?></div>
                                        </td>
                                        <td><?= $row->no_rangka; ?></td>
                                        <td><?= $row->tgl_estimasi_selesai; ?></td>
                                        <td>
                                            <?php
                                            if (empty($row->tgl_selesai)) :
                                            ?>
                                                <a>-</a>
                                            <?php else : ?>
                                                <?= $row->tgl_selesai; ?>
                                            <?php endif; ?>
                                        </td>
                                        <?php
                                        if (empty($row->tgl_selesai)) :
                                            $class = '';
                                        else :
                                            if ($row->hari_faktur == 0) {
                                                $class = 'table-success';
                                            } else if ($row->hari_faktur < 0) {
                                                $class = 'table-primary';
                                            } else {
                                                $class = 'table-warning';
                                            }
                                        endif; ?>
                                        <td class="<?= $class; ?>">
                                            <?php
                                            if (empty($row->tgl_selesai)) :
                                            ?>
                                                <a href="<?= site_url('marketing/faktur/update/selesai/' . $row->id_proses) ?>"><i class="fas fa-check"></i> Selesai</a> | <a href="javascript:void(0);" onclick="hapus('<?= $row->id_faktur; ?>')"><i class="fa fa-trash" aria-hidden="true" style="color: red;"></i></a>
                                            <?php else :
                                                if ($row->hari_faktur == 0) {
                                                    echo "Tepat waktu";
                                                } else if ($row->hari_faktur < 0) {
                                                    echo "Lebih awal " . $row->hari_faktur . " hari";
                                                } else {

                                                    echo "Terlambat " . $row->hari_faktur . " hari";
                                                }
                                            endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tile-footer">
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function hapus(id) {
        Swal.fire({
            title: 'Anda yakin?',
            text: "Akan menghapus data ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus ini!'
        }).then((result) => {
            if (result.value) {
                location.href = "<?= site_url('marketing/faktur/delete/faktur_kendaraan/') ?>" + id;
            }
        })
    }
</script>