<div class="app-title">
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-mmksi.png'); ?>" alt="logo-mmksi" width="50px">
    </div>
    <div class="text-center">
        <h1> <?= $page_header; ?></h1>
        <p>Sales Management System</p>
    </div>
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-fuso.png'); ?>" alt="logo-fuso" width="50px">
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <a href="<?= site_url('marketing/karoseri/baru') ?>" class="btn btn-sm btn-primary text-white">Tambah Baru</a>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                                <tr class="text-center">
                                    <th>Kode Karoseri</th>
                                    <th>Nama Karoseri</th>
                                    <th>Estimasi Hari</th>
                                    <th>Keterangan</th>
                                    <th width="1px">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Fetch data dari database-->
                                <?php foreach ($data->result() as $row) : ?>
                                    <tr class="uppercase">
                                        <td><?php echo $row->kd_karoseri; ?></td>
                                        <td><?php echo $row->nm_karoseri; ?></td>
                                        <td><?php echo $row->estimasi_hari; ?> Hari</td>
                                        <td><?php echo $row->keterangan; ?></td>
                                        <td><a href="javascript:void(0)" onclick="hapus('<?= $row->kd_karoseri; ?>')"><i class="fas fa-trash" style="color: red;"></i></a></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tile-footer">
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function hapus(id) {
        Swal.fire({
            title: 'Anda yakin?',
            text: "Akan menghapus data ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus ini!'
        }).then((result) => {
            if (result.value) {
                location.href = "<?= site_url('marketing/karoseri/delete/karoseri/') ?>" + id;
            }
        })
    }
</script>