<div class="app-title">
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-mmksi.png'); ?>" alt="logo-mmksi" width="50px">
    </div>
    <div class="text-center">
        <h1> <?= $page_header; ?></h1>
        <p>Sales Management System</p>
    </div>
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-fuso.png'); ?>" alt="logo-fuso" width="50px">
    </div>
</div>
<form method="POST" id="form-konsumen" action="<?= site_url('marketing/karoseri/create/karoseri'); ?>" autocomplete="off">
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-title-w-btn">
                        <h5>Form Karoseri<h5>
                    </div>
                    <div class="tile-body">
                        <input type="hidden" name="id" id="id">

                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="kd_karoseri">Kode Karoseri</label>
                            <div class="col-md-9 input-group">
                                <input type="text" name="kd_karoseri" class="form-control form-control-sm" minlength="4" maxlength="4" id="kd_karoseri" placeholder="Kode Karoseri (*)" autofocus required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="nm_karoseri">Nama Karoseri</label>
                            <div class="col-md-9 input-group">
                                <input type="text" name="nm_karoseri" class="form-control form-control-sm" id="nm_karoseri" placeholder="Nama Karoseri (*)" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="estimasi_hari">Estimasi Hari</label>
                            <div class="col-md-9 input-group">
                                <input type="number" name="estimasi_hari" class="form-control form-control-sm" id="estimasi_hari" min="0" value="0" required>
                            </div>
                        </div>
                        <div class="form-group-textarea row">
                            <label class="control-label col-md-3 float-right small" for="keterangan">Keterangan</label>
                            <div class="col-md-9 input-group">
                                <textarea name="keterangan" class="form-control form-control-sm" id="keterangan" placeholder="Keterangan"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary btn-block" id="btn-simpan" form="form-konsumen">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>