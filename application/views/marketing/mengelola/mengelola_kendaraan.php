<div class="app-title">
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-mmksi.png'); ?>" alt="logo-mmksi" width="50px">
    </div>
    <div class="text-center">
        <h1> <?= $page_header; ?></h1>
        <p>Sales Management System</p>
    </div>
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-fuso.png'); ?>" alt="logo-fuso" width="50px">
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-title-w-btn">
                    <h5>Mengelola Kendaraan<h5>
                </div>
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                                <tr class="text-center">
                                    <th>Nama Konsumen</th>
                                    <th>Merk</th>
                                    <th>Tipe</th>
                                    <th>Warna</th>
                                    <th>No Rangka</th>
                                    <th>Karoseri</th>
                                    <th>Tipe Rear Body</th>
                                    <th>Tgl Estimasi STNK</th>
                                    <th>Tgl Selesai STNK</th>
                                    <th>Tgl Estimasi Karoseri</th>
                                    <th>Tgl Selesai Karoseri</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Fetch data dari database-->
                                <?php
                                foreach ($data->result() as $row) :
                                    $dt_konsumen = $this->crud_model->read('dt_konsumen', ['id_konsumen' => $row->id_konsumen])->row();
                                ?>
                                    <tr class="uppercase">
                                        <td>[<b><?php echo $dt_konsumen->jenis; ?></b>] <?php echo $dt_konsumen->nm_lengkap; ?></td>
                                        <td><?= $row->merk; ?></td>
                                        <td><?= $row->tipe; ?></td>
                                        <td><?= $row->warna; ?></td>
                                        <td><?= $row->no_rangka; ?></td>
                                        <?php
                                        if ($row->kd_karoseri == 'NONK') :
                                        ?>
                                            <td colspan="2" class="table-secondary" align="center">-</td>
                                        <?php else : ?>
                                            <td><?= $row->nm_karoseri; ?></td>
                                            <td><?= $row->tipe_rear_body; ?></td>
                                        <?php endif; ?>
                                        <td align="center"><?= $row->tgl_estimasi_stnk; ?></td>
                                        <?php
                                        if (!empty($row->tgl_selesai)) :
                                        ?>
                                            <td align="center"><?php if ($row->tgl_stnk == NULL) {
                                                                    echo '<input type="checkbox" onclick="selesai_stnk(\'' . $row->id_proses . '\')">';
                                                                } else {
                                                                    echo $row->tgl_stnk;
                                                                } ?>
                                            </td>
                                        <?php else : ?>
                                            <td> - </td>
                                        <?php endif; ?>
                                        <?php
                                        if ($row->kd_karoseri == 'NONK') :
                                        ?>
                                            <td colspan="2" class="table-secondary" align="center">-</td>
                                        <?php else : ?>
                                            <td align="center"><?= $row->tgl_estimasi_karoseri; ?></td>
                                            <?php if (!empty($row->tgl_selesai)) : ?>
                                                <td align="center"><?php if ($row->tgl_karoseri == NULL) {
                                                                        echo '<input type="checkbox" onclick="selesai_karoseri(\'' . $row->id_proses . '\')">';
                                                                    } else {
                                                                        echo $row->tgl_karoseri;
                                                                    } ?>
                                                </td>
                                            <?php else : ?>
                                                <td>-</td>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tile-footer">
                    <?php echo $pagination; ?>
                </div>
                <a class="btn btn-sm btn-primary text-white" href="<?= site_url('marketing/karoseri/data') ?>">Daftar Karoseri</a>
            </div>
        </div>
    </div>
</div>

<script>
    function selesai_stnk(id) {
        location.href = "<?= site_url('marketing/mengelola/update/selesai_stnk/') ?>" + id;
    }

    function selesai_karoseri(id) {
        location.href = "<?= site_url('marketing/mengelola/update/selesai_karoseri/') ?>" + id;
    }
</script>