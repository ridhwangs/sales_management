<div class="app-title">
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-mmksi.png'); ?>" alt="logo-mmksi" width="50px">
    </div>
    <div class="text-center">
        <h1> <?= $page_header; ?></h1>
        <p>Sales Management System</p>
    </div>
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-fuso.png'); ?>" alt="logo-fuso" width="50px">
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-title-w-btn">
                    <h5>Data SPK<h5>
                </div>
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                                <tr class="text-center">
                                    <th>No SPK</th>
                                    <th>Tanggal</th>
                                    <th>Nama Konsumen</th>
                                    <th>Sumber Informasi</th>
                                    <th>Tipe Kedatangan</th>
                                    <th>Merk Kendaraan Saat ini</th>
                                    <th>Tipe Identitas STNK</th>
                                    <th>No Identitas STNK</th>
                                    <th>Nama STNK</th>
                                    <th>Industri</th>
                                    <th>Catatan</th>
                                    <th>Status</th>
                                    <th width="1px">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Fetch data dari database-->
                                <?php
                                foreach ($data->result() as $row) :
                                    $dt_konsumen = $this->crud_model->read('dt_konsumen', ['id_konsumen' => $row->id_konsumen])->row();
                                ?>
                                    <tr class="uppercase">
                                        <td><?php echo $row->no_spk; ?></td>
                                        <td><?php echo $row->tanggal; ?></td>
                                        <td>[<b><?php echo $dt_konsumen->jenis; ?></b>] <?php echo $dt_konsumen->nm_lengkap; ?></td>
                                        <td><?php echo $row->sumber_informasi; ?></td>
                                        <td><?php echo $row->tipe_kedatangan; ?></td>
                                        <td><?php echo $row->merk_kendaraan_saat_ini; ?></td>
                                        <td><?php echo $row->tipe_identitas_stnk; ?></td>
                                        <td><?php echo $row->no_identitas_stnk; ?></td>
                                        <td><?php echo $row->nm_stnk; ?></td>
                                        <td><?php echo $row->industri; ?></td>
                                        <td><?php echo $row->catatan; ?></td>
                                        <td><?php echo $row->status; ?></td>
                                        <td>
                                            <?php if ($row->status == 'approve') : ?>
                                                <?php
                                                $num_proses = $this->crud_model->read('proses_kendaraan', ['id_spk' => $row->id_spk])->num_rows();
                                                if ($num_proses > 0) :
                                                ?>
                                                    <a href="<?= site_url('marketing/faktur/kendaraan/' . $row->id_spk); ?>">Lihat Invoice</a>
                                                <?php
                                                else :
                                                ?>
                                                    <a href="<?= site_url('marketing/faktur/kendaraan/' . $row->id_spk); ?>">Proses Faktur</a>
                                                <?php
                                                endif;
                                                ?>
                                            <?php else : ?>
                                                <a href="<?= site_url('marketing/spk/form_edit/' . $row->id_spk); ?>"><i class="fas fa-pencil-alt"></i></a>
                                                <a href="javascript:void(0)" onclick="hapus('<?= $row->id_spk; ?>')"><i class="fas fa-trash" style="color: red;"></i></a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tile-footer">
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function hapus(id) {
        Swal.fire({
            title: 'Anda yakin?',
            text: "Akan menghapus data ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus ini!'
        }).then((result) => {
            if (result.value) {
                location.href = "<?= site_url('marketing/spk/delete/spk/') ?>" + id;
            }
        })
    }
</script>