<div class="app-title">
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-mmksi.png'); ?>" alt="logo-mmksi" width="50px">
    </div>
    <div class="text-center">
        <h1> <?= $page_header; ?></h1>
        <p>Sales Management System</p>
    </div>
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-fuso.png'); ?>" alt="logo-fuso" width="50px">
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-title-w-btn">
                    <h5>Data Konsumen<h5>
                </div>
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                                <tr class="text-center">
                                    <th>Jenis</th>
                                    <th>Nama Lengkap</th>
                                    <th>Alamat</th>
                                    <th>No Handphone</th>
                                    <th>No Telp</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Alamat</th>
                                    <th>Kota</th>
                                    <th>Provinsi</th>
                                    <th>Kode Pos</th>
                                    <th>Status</th>
                                    <th>Topik</th>
                                    <th width="1px">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Fetch data dari database-->
                                <?php foreach ($data->result() as $row) : ?>
                                    <tr class="uppercase">
                                        <td><?php echo $row->jenis; ?></td>
                                        <td><?php echo $row->nm_lengkap; ?></td>
                                        <td><?php echo $row->alamat; ?></td>
                                        <td><?php echo $row->no_handphone; ?></td>
                                        <td><?php echo $row->no_telp; ?></td>
                                        <td><?php echo $row->email; ?></td>
                                        <td><?php if ($row->jenis_kelamin == 'p') {
                                                echo "Pria";
                                            } else {
                                                echo "Wanita";
                                            } ?></td>
                                        <td><?php echo $row->alamat; ?></td>
                                        <td><?php echo $row->kota; ?></td>
                                        <td><?php echo $row->provinsi; ?></td>
                                        <td><?php echo $row->kode_pos; ?></td>
                                        <td><?php echo $row->status; ?></td>
                                        <td><?php echo $row->topik; ?></td>
                                        <td>
                                            <a href="<?= site_url('marketing/konsumen/form/' . $row->id_konsumen); ?>"><i class="fas fa-user-edit"></i></a>
                                            <?php
                                            if ($row->status == 'prospek') :
                                            ?>
                                                | <a href="<?= site_url('marketing/spk/form/' . $row->id_konsumen); ?>"><i class="fab fa-wpforms"></i> Form SPK</a>
                                            <?php endif; ?>
                                            | <a href="javascript:void(0)" onclick="hapus('<?= $row->id_konsumen; ?>')"><i class="fas fa-trash" style="color: red;"></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tile-footer">
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function hapus(id) {
        Swal.fire({
            title: 'Anda yakin?',
            text: "Akan menghapus data ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus ini!'
        }).then((result) => {
            if (result.value) {
                location.href = "<?= site_url('marketing/konsumen/delete/konsumen/') ?>" + id;
            }
        })
    }
</script>