<div class="app-title">
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-mmksi.png'); ?>" alt="logo-mmksi" width="50px">
    </div>
    <div class="text-center">
        <h1> <?= $page_header; ?></h1>
        <p>Sales Management System</p>
    </div>
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-fuso.png'); ?>" alt="logo-fuso" width="50px">
    </div>
</div>
<form method="POST" id="form-konsumen" action="<?= site_url('marketing/konsumen/create/konsumen'); ?>" autocomplete="off">
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-title-w-btn">
                        <h5>Form Konsumen<h5>
                    </div>
                    <div class="tile-body">
                        <input type="hidden" name="id_konsumen" id="id_konsumen">
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="jenis">Jenis</label>
                            <div class="col-md-9 input-group">
                                <select name="jenis" class="form-control form-control-sm" id="jenis" required>
                                    <option value="" selected disabled>Silahkan Pilih</option>
                                    <option value="perusahaan">1. Perusahaan</option>
                                    <option value="perorangan">2. Perorangan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="nm_lengkap">Nama Lengkap</label>
                            <div class="col-md-9 input-group">
                                <input type="text" name="nm_lengkap" class="form-control form-control-sm" id="nm_lengkap" placeholder="Nama Lengkap (*)" autofocus required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="no_handphone">No Handphone</label>
                            <div class="col-md-9 input-group">
                                <input type="text" name="no_handphone" class="form-control form-control-sm" id="no_handphone" placeholder="No Handphone (*)" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="no_telp">No Telp</label>
                            <div class="col-md-9 input-group">
                                <input type="text" name="no_telp" class="form-control form-control-sm" id="no_telp" placeholder="No Telp">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="email">Email</label>
                            <div class="col-md-9 input-group">
                                <input type="email" name="email" class="form-control form-control-sm" id="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="jenis_kelamin">Jenis Kelamin</label>
                            <div class="col-md-9 input-group">
                                <select name="jenis_kelamin" class="form-control form-control-sm" id="jenis_kelamin" required>
                                    <option value="" selected disabled>Silahkan Pilih</option>
                                    <option value="p">1. Pria</option>
                                    <option value="w">2. Wanita</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group-textarea row">
                            <label class="control-label col-md-3 float-right small" for="alamat">Alamat</label>
                            <div class="col-md-9 input-group">
                                <textarea name="alamat" class="form-control form-control-sm" id="alamat" placeholder="Alamat"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="kota">Kota</label>
                            <div class="col-md-9 input-group">
                                <input type="text" name="kota" class="form-control form-control-sm" id="kota" placeholder="Kota">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="provinsi">Provinsi</label>
                            <div class="col-md-9 input-group">
                                <input type="text" name="provinsi" class="form-control form-control-sm" id="provinsi" placeholder="Provinsi">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="kode_pos">Kode Pos</label>
                            <div class="col-md-9 input-group">
                                <input type="text" name="kode_pos" class="form-control form-control-sm" id="kode_pos" placeholder="Kode Pos">
                            </div>
                        </div>
                        <div class="form-group-textarea row">
                            <label class="control-label col-md-3 float-right small" for="topik">Topik</label>
                            <div class="col-md-9 input-group">
                                <textarea name="topik" class="form-control form-control-sm" id="topik" placeholder="Topik"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="status">Status</label>
                            <div class="col-md-9 input-group">
                                <select name="status" class="form-control form-control-sm" id="status" required>
                                    <option value="" selected disabled>Silahkan Pilih</option>
                                    <option value="prospek">1. Prospek</option>
                                    <option value="suspek">2. Suspek</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary btn-block" id="btn-simpan" form="form-konsumen">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php
if (!empty($this->uri->segment(4))) :
?>
    <script>
        $(document).ready(function() {
            $('.readonly').find('input, textarea, select').attr('disabled', 'disabled');
            $("#id_konsumen").val('<?= $data_konsumen->id_konsumen; ?>');
            $("#jenis").val('<?= $data_konsumen->jenis; ?>');
            $("#nm_lengkap").val('<?= $data_konsumen->nm_lengkap; ?>');
            $("#no_handphone").val('<?= $data_konsumen->no_handphone; ?>');
            $("#no_telp").val('<?= $data_konsumen->no_telp; ?>');
            $("#email").val('<?= $data_konsumen->email; ?>');
            $("#jenis_kelamin").val('<?= $data_konsumen->jenis_kelamin; ?>');
            $("#alamat").val('<?= $data_konsumen->alamat; ?>');
            $("#kota").val('<?= $data_konsumen->kota; ?>');
            $("#provinsi").val('<?= $data_konsumen->provinsi; ?>');
            $("#kode_pos").val('<?= $data_konsumen->kode_pos; ?>');
            $("#topik").val('<?= $data_konsumen->topik; ?>');
            $("#status").val('<?= $data_konsumen->status; ?>');
        });
    </script>
<?php endif; ?>