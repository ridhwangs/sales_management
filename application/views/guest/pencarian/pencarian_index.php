<style>
  .carousel {
    width: 640px;
    height: 360px;
  }
</style>
<div class="app-title">
  <div class="tile-title">
    <img src="<?= base_url('assets/images/logo-mmksi.png'); ?>" alt="logo-mmksi" width="50px">
  </div>
  <div class="text-center">
    <h1>Pencarian Kendaraan</h1>
    <span><a href="<?= site_url('dokumen') ?>">Download Brosur</a> | <a href="<?= site_url('auth/logout') ?>">Login</a></span>
  </div>
  <div class="tile-title">
    <img src="<?= base_url('assets/images/logo-fuso.png'); ?>" alt="logo-fuso" width="50px">
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <div class="container">
          <div class="col-md-6 mx-auto">

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <?php for ($i = 2; $i < 20; $i++) : ?>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <?php endfor; ?>

              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <div class="container">
                    <img src="<?= base_url('assets/images/product/1.jpeg') ?>" class="d-block w-100" alt="...">
                  </div>
                </div>
                <?php for ($i = 2; $i < 20; $i++) : ?>
                  <div class="carousel-item">
                    <div class="container">
                      <img src="<?= base_url('assets/images/product/' . $i . '.jpeg') ?>" class="d-block w-100" alt="...">
                    </div>
                  </div>
                <?php endfor; ?>
              </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
        </div>
        <form method="GET">
          <div class="col-m-12">
            <div class="form-group row">
              <div class="col-md-12 input-group">
                <input type="text" name="no_rangka" class="form-control uppercase" id="no_rangka" placeholder="Silahkan cari berdasarkan No. Rangka Kendaraan..." autofocus required>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <button class="btn btn-sm btn-success btn-block" form="form-pencarian">Cari Kendaraan</button>
              </div>
            </div>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>