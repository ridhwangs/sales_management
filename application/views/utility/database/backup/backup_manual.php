<div class="row">
    <div class="col-md-6 container">
        <div class="tile">
            <div class="tile-title-w-btn">
                <h3 class="title">Manual Database</h3>
            </div>
            <hr>
            <div class="tile-body">
                <p><a class="btn btn-primary icon-btn btn-block" href="javascript:void(0)" onclick="backup();"><i class="fa fa-database"></i>Backup Database</a></p>
            </div>
        </div>
    </div>

    <script>
        function backup() {
            $('#cover-spin').show(0);
            if (confirm('Anda yakin akan backup Database?')) {
                location.href = "<?= site_url('utility/database/backup/create/backup') ?>";
                $.notify({
                    title: "Berhasil",
                    message: "Backup database berhasil..",
                    icon: "success"
                }, {
                    type: "success",
                });

            } else {
                $('#cover-spin').hide(0);

            }
        }
    </script>