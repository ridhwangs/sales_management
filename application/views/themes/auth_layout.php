<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="Hexacode System">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@ridhwangs">
    <meta property="twitter:creator" content="@ridhwangs">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Smartcode - TAMAN KOPO INDAH.">
    <meta property="og:title" content="Smartcode - TAMAN KOPO INDAH.">

    <meta property="og:description" content="Interface System">
    <title>Sales Management System - <?= $page_header; ?></title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <script src="<?= base_url(); ?>node_modules/jquery/dist/jquery.min.js"></script>

    <!-- Main CSS-->
    <link rel="stylesheet" href="<?= base_url(); ?>node_modules/bootstrap/dist/css/bootstrap.css" crossorigin="anonymous">
    <script src="<?= base_url(); ?>node_modules/bootstrap/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>

    <!-- pace loading -->
    <link rel="stylesheet" href="<?= assets_url(); ?>vendor/pace/v1.0.0/themes/green/pace-theme-minimal.css" crossorigin="anonymous" />

    <!-- Font-icon css-->
    <link rel="stylesheet" href="<?= assets_url(); ?>vendor/fontawesome-free-5.11.2-web/css/all.min.css" crossorigin="anonymous">


    <link rel="stylesheet" href="<?= assets_url(); ?>vendor/animate/animate.css" />

    <!-- <script src="<?= base_url(); ?>node_modules/jquery-tabledit-1.2.3/jquery.tabledit.js"></script> -->

    <link rel="stylesheet" type="text/css" href="<?= assets_url(); ?>css/main.css">
    <link rel="stylesheet" type="text/css" href="<?= assets_url(); ?>css/auth.css">
</head>

<body class="app" data-senna="data-senna" data-senna-surface="data-senna-surface">
    <ul class="cb-slideshow">
        <li><span></span>

        </li>
        <li><span></span>

        </li>
        <li><span></span>

        </li>
        <li><span></span>

        </li>
        <li><span></span>

        </li>
    </ul>
    <section class="login-content bg">
        <?= $output; ?>
    </section>

    <!-- The javascript plugin to display page loading on top-->
    <script data-pace-options='{ "ajax": false }' src="<?= assets_url(); ?>vendor/pace/v1.0.0/pace.min.js" crossorigin="anonymous"></script>

    <!-- Page specific javascripts-->
    <script type="text/javascript" src="<?= assets_url(); ?>js/plugins/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="<?= assets_url(); ?>js/modernizr.custom.86080.js"></script>

</body>

</html>