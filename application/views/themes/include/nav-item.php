<li class="nav-item dropdown"> <span class="app-nav__item uppercase" data-toggle="dropdown" aria-label="Open Profile Menu"> <a href="javascript:void(0);"><i class="fa fa-user fa-lg"></i></a></span>
  <ul class="dropdown-menu settings-menu dropdown-menu-right">
    <li><?= anchor('auth/logout', '<i class="fas fa-sign-out-alt"></i> Logout', 'class="dropdown-item"') ?></li>
  </ul>
</li>