<!DOCTYPE html>
<html lang="en">

<head>
  <meta name="description" content="Hexacode System">
  <!-- Twitter meta-->
  <meta property="twitter:card" content="summary_large_image">
  <meta property="twitter:site" content="@ridhwangs">
  <meta property="twitter:creator" content="@ridhwangs">
  <!-- Open Graph Meta-->
  <meta property="og:type" content="website">
  <meta property="og:site_name" content="Smartcode - TAMAN KOPO INDAH.">
  <meta property="og:title" content="Smartcode - TAMAN KOPO INDAH.">

  <meta property="og:description" content="Interface System">
  <title>Sales Management System - <?= $page_header; ?></title>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <script src="<?= base_url(); ?>node_modules/jquery/dist/jquery.min.js"></script>

  <!-- Main CSS-->
  <link rel="stylesheet" href="<?= base_url(); ?>node_modules/bootstrap/dist/css/bootstrap.css" crossorigin="anonymous">
  <script src="<?= base_url(); ?>node_modules/bootstrap/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>

  <!-- pace loading -->
  <link rel="stylesheet" href="<?= assets_url(); ?>vendor/pace/v1.0.0/themes/green/pace-theme-minimal.css" crossorigin="anonymous" />

  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?= assets_url(); ?>vendor/fontawesome-free-5.11.2-web/css/all.min.css" crossorigin="anonymous">

  <link rel="stylesheet" href="<?= assets_url(); ?>vendor/select2-4.0.11/dist/css/select2.min.css" crossorigin="anonymous" />
  <link rel="stylesheet" href="<?= base_url(); ?>node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css" crossorigin="anonymous" />

  <link rel="stylesheet" href="<?= base_url(); ?>node_modules/sweetalert2/dist/sweetalert2.min.css" />

  <link rel="stylesheet" href="<?= assets_url(); ?>vendor/animate/animate.css" />

  <!-- <script src="<?= base_url(); ?>node_modules/jquery-tabledit-1.2.3/jquery.tabledit.js"></script> -->

  <link rel="stylesheet" type="text/css" href="<?= assets_url(); ?>css/main.css">
  <link rel="stylesheet" type="text/css" href="<?= assets_url(); ?>js/plugins/fixedColumns.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="<?= assets_url(); ?>vendor/jquery-datatables-checkboxes-1.2.11/css/dataTables.checkboxes.css">
  <script src="<?= assets_url(); ?>vendor/select2-4.0.11/dist/js/select2.full.min.js" crossorigin="anonymous"></script>
  <script src="<?= assets_url(); ?>vendor/printThis/printThis.js"></script>
  <style>
    table.dataTable.table-striped.DTFC_Cloned tbody tr:nth-of-type(odd) {
      background-color: #F3F3F3;
    }

    table.dataTable.table-striped.DTFC_Cloned tbody tr:nth-of-type(even) {
      background-color: white;
    }
  </style>
</head>

<body class="app sidenav-toggled rtl" data-senna="data-senna" data-senna-surface="data-senna-surface">
  <?php
  $user = $this->ion_auth->user()->row();
  ?>
  <div id="cover-spin"></div>
  <!-- Navbar-->
  <footer class="fixed-bottom" style="border-top: 1px solid red;">
    <div class="container-fluid bg-light">
      <ul class="navbar-nav">
        <li class="nav-item text-right">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo (ENVIRONMENT === 'development') ? 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></li>
      </ul>
    </div>
  </footer>

  <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-danger app-header">

    <a class="app-sidebar__toggle" href="javascript:void(0)" data-toggle="sidebar" aria-label="Hide Sidebar" onclick="w3_open()">
      <i class="fa fa-bars" style="font-size:30px; padding: 10px;"></i>
    </a>

    <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button> -->
    <ul class="navbar-nav">
      <span class="text-white font-12 uppercase">MARKETING - <?= $page_header; ?></span>
    </ul>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto" id="menu-atas">

      </ul>

      <ul class="navbar-nav ml-auto">
        <?php
        $this->load->view('themes/include/nav-item');
        ?>
      </ul>
    </div>
  </nav>
  <div>
    <aside class="app-sidebar" style="z-index:99999" id="mySidebar">
      <ul class="app-menu" style="padding-bottom:20px;">
        <a href="javascript:void(0)" data-toggle="sidebar" onclick="w3_close()" style="background: #e84118;text-align:right; padding:3px 3px;" class="app-menu__item treeview-indicator">
          <span aria-hidden="true" class="app-menu__label">Tutup (X)</span>
        </a>
      </ul>
      <div class="app-sidebar__user">
        <img class="app-sidebar__user-avatar" src="<?= assets_url(); ?>img/account.png" alt="User Image">
        <div>
          <p class="app-sidebar__user-name"><?= $user->first_name; ?></p>
          <p class="app-sidebar__user-designation"><?= $user->email; ?></p>
          <hr style="border-top: 1px dotted red; margin-top:5px; margin-bottom:1px;">
          <p class="app-sidebar__user-designation">
        </div>
      </div>
      <!-- Page Content -->

      <!-- START UL MENU -->
      <ul class="app-menu small uppercase">
        <li>
          <a href="<?= site_url('marketing/beranda') ?>" class="app-menu__item">
            <i class="app-menu__icon fab fa-accusoft"></i>
            <span class="app-menu__label">Beranda</span></a>
        </li>
        <li class="treeview "><a class="app-menu__item" href="javascript:void(0)" data-toggle="treeview"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Daftar Konsumen</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="<?= site_url('marketing/konsumen/baru') ?>"><i class="icon fa fa-circle-o"></i> Daftar Baru</a></li>
            <li><a class="treeview-item" href="<?= site_url('marketing/konsumen/data') ?>"><i class="icon fa fa-circle-o"></i> Data Konsumen</a></li>
          </ul>
        </li>
        <li>
          <a href="<?= site_url('marketing/spk/data') ?>" class="app-menu__item">
            <i class="app-menu__icon fas fa-file-alt"></i>
            <span class="app-menu__label">Mengelola SPK</span></a>
        </li>
        <li>
          <a href="<?= site_url('marketing/faktur/faktur_kendaraan') ?>" class="app-menu__item">
            <i class="app-menu__icon fas fa-file-alt"></i>
            <span class="app-menu__label">Faktur Kendaraan</span></a>
        </li>
        <li>
          <a href="<?= site_url('marketing/mengelola/kendaraan') ?>" class="app-menu__item">
            <i class="app-menu__icon fas fa-file-alt"></i>
            <span class="app-menu__label">Mengelola Kendaraan</span></a>
        </li>
        <!-- <li class="treeview "><a class="app-menu__item" href="javascript:void(0)" data-toggle="treeview"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Daftar Karoseri</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="<?= site_url('marketing/karoseri/baru') ?>"><i class="icon fa fa-circle-o"></i> Daftar Baru</a></li>
            <li><a class="treeview-item" href="<?= site_url('marketing/karoseri/data') ?>"><i class="icon fa fa-circle-o"></i> Data Karoseri</a></li>
          </ul>
        </li> -->
        <li>
          <a href="<?= site_url('auth/logout') ?>" class="app-menu__item">
            <i class="app-menu__icon fas fa-sign-out-alt"></i>
            <span class="app-menu__label">Logout</span></a>
        </li>
      </ul>
      <!-- END UL MENU -->
    </aside>
  </div>

  <div class="w3-overlay w3-animate-opacity" data-toggle="sidebar" onclick="w3_close()" style="cursor:pointer;" id="myOverlay"></div>

  <main class="app-content" role="main">
    <?= $output; ?>
  </main> <!-- The javascript plugin to display page loading on top-->
  <script data-pace-options='{ "ajax": false }' src="<?= assets_url(); ?>vendor/pace/v1.0.0/pace.min.js" crossorigin="anonymous"></script>

  <!-- Data table plugin-->
  <script type="text/javascript" src="<?= assets_url(); ?>js/plugins/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?= assets_url(); ?>js/plugins/dataTables.bootstrap.min.js"></script>
  <script type="text/javascript" src="<?= assets_url(); ?>vendor/jquery-datatables-checkboxes-1.2.11/js/dataTables.checkboxes.js"></script>

  <script type="text/javascript" src="<?= assets_url(); ?>js/plugins/dataTables.fixedColumns.min.js"></script>

  <!-- InputMask -->
  <script src="<?= assets_url(); ?>vendor/jquery-maskedinput/dist/jquery.maskedinput.js"></script>

  <!-- Vendor plugin-->
  <script src="<?= assets_url(); ?>vendor/autoNumeric/autoNumeric.js"></script>


  <!-- Page specific javascripts-->
  <script type="text/javascript" src="<?= assets_url(); ?>js/plugins/bootstrap-notify.min.js"></script>
  <script src="<?= assets_url(); ?>js/main.js"></script>

  <script src="<?= base_url(); ?>node_modules/sweetalert2/dist/sweetalert2.min.js" crossorigin="anonymous"></script>
  <script>
    var popupBlockerChecker = {
      check: function(popup_window) {
        var scope = this;
        if (popup_window) {
          if (/chrome/.test(navigator.userAgent.toLowerCase())) {
            setTimeout(function() {
              scope.is_popup_blocked(scope, popup_window);
            }, 200);
          } else {
            popup_window.onload = function() {
              scope.is_popup_blocked(scope, popup_window);
            };
          }
        } else {
          scope.displayError();
        }
      },
      is_popup_blocked: function(scope, popup_window) {
        if ((popup_window.innerHeight > 0) == false) {
          scope.displayError();
        }
      },
      displayError: function() {
        $('#cover-spin').hide(0);
      }
    };
  </script>
  <?php if (!empty($this->session->flashdata('message'))) : ?>
    <script>
      $(document).ready(function() {
        $.notify({
          title: "Info",
          message: "<?php echo $this->session->flashdata('message'); ?>",
          icon: "info"
        }, {
          type: "info",
        });
      });
    </script>
  <?php endif; ?>
</body>

</html>