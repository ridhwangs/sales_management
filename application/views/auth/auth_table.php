<div class="app-title">
	<div class="tile-title">
		<img src="<?= base_url('assets/images/logo-mmksi.png'); ?>" alt="logo-mmksi" width="50px">
	</div>
	<div class="text-center">
		<h1> <?= $page_header; ?></h1>
		<p>Sales Management System</p>
	</div>
	<div class="tile-title">
		<img src="<?= base_url('assets/images/logo-fuso.png'); ?>" alt="logo-fuso" width="50px">
	</div>
</div>
<div class="row">
	<div class="col-md-8 container">
		<div class="tile">
			<p><?php echo anchor('auth/create_user', lang('index_create_user_link')) ?></p>
			<hr>
			<div class="tile-body">
				<div class="table-responsive-sm">
					<table class="table table-sm table-hover table-bordered">
						<thead class="thead-dark">
							<th><?php echo lang('index_fname_th'); ?></th>
							<th><?php echo lang('index_lname_th'); ?></th>
							<th>Username</th>
							<th><?php echo lang('index_email_th'); ?></th>
							<th>Jabatan</th>
							<th width="1px"><?php echo lang('index_status_th'); ?></th>
							<th width="1px"><?php echo lang('index_action_th'); ?></th>
							<th>#</th>
						</thead>
						<?php foreach ($users as $user) : ?>
							<tr>
								<td><?php echo htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8'); ?></td>
								<td><?php echo htmlspecialchars($user->last_name, ENT_QUOTES, 'UTF-8'); ?></td>
								<td><?php echo htmlspecialchars($user->username, ENT_QUOTES, 'UTF-8'); ?></td>
								<td><?php echo htmlspecialchars($user->email, ENT_QUOTES, 'UTF-8'); ?></td>
								<td><?php echo htmlspecialchars($user->role, ENT_QUOTES, 'UTF-8'); ?></td>
								<td><?php echo ($user->active) ? anchor("auth/deactivate/" . $user->id, lang('index_active_link')) : anchor("auth/activate/" . $user->id, lang('index_inactive_link')); ?></td>
								<td><?php echo anchor("auth/edit_user/" . $user->id, 'Edit'); ?></td>
								<td><a href="javascript:void(0)" onclick="hapus('<?= $user->id; ?>')"><i class="fas fa-trash" style="color: red;"></i></a></td>

							</tr>
						<?php endforeach; ?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function hapus(id) {
		Swal.fire({
			title: 'Anda yakin?',
			text: "Akan menghapus data ini!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, hapus ini!'
		}).then((result) => {
			if (result.value) {
				location.href = "<?= site_url('auth/hapus/') ?>" + id;
			}
		})
	}
</script>