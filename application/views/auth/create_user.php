<div class="app-title">
      <div class="tile-title">
            <img src="<?= base_url('assets/images/logo-mmksi.png'); ?>" alt="logo-mmksi" width="50px">
      </div>
      <div class="text-center">
            <h1> <?= $page_header; ?></h1>
            <p>Sales Management System</p>
      </div>
      <div class="tile-title">
            <img src="<?= base_url('assets/images/logo-fuso.png'); ?>" alt="logo-fuso" width="50px">
      </div>
</div>
<div class="row">
      <div class="col-md-8 container">
            <div class="tile">
                  <div class="tile-title-w-btn">
                        <h3><?php echo lang('create_user_heading'); ?></h3>
                        <a href="<?= site_url('auth') ?>" class="btn btn-sm btn-danger">[x] Tutup</a>
                  </div>
                  <div id="infoMessage"><?php echo $message; ?></div>
                  <hr>
                  <?php echo form_open("auth/create_user"); ?>
                  <div class="tile-body">
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="role">Jabatan</label>
                              <div class="col-md-9 input-group">
                                    <select name="role" class="form-control" id="role" required>
                                          <option value="" selected disabled>--Silahkan pilih:</option>
                                          <option value="supervisor">Supervisor</option>
                                          <option value="marketing">Marketing</option>
                                    </select>
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="no_identitas">No Identitas</label>
                              <div class="col-md-9 input-group">
                                    <input type="text" name="no_identitas" value="" id="no_identitas" class="form-control">
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="first_name"><?php echo lang('create_user_fname_label', 'first_name'); ?></label>
                              <div class="col-md-9 input-group">
                                    <input type="text" name="first_name" value="" id="first_name" class="form-control">
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="last_name"><?php echo lang('create_user_lname_label', 'last_name'); ?></label>
                              <div class="col-md-9 input-group">
                                    <input type="text" name="last_name" value="" id="last_name" class="form-control">
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="identity">Username</label>
                              <div class="col-md-9 input-group">
                                    <?php
                                    if ($identity_column !== 'email') {
                                          echo '<p>';
                                          echo form_error('identity');
                                          echo '</p>';
                                    }
                                    ?>
                                    <input type="text" name="identity" value="" id="identity" class="form-control" required>
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="company"><?php echo lang('create_user_company_label', 'company'); ?></label>
                              <div class="col-md-9 input-group">
                                    <input type="text" name="company" value="" id="company" class="form-control">
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="email"><?php echo lang('create_user_email_label', 'email'); ?></label>
                              <div class="col-md-9 input-group">
                                    <input type="text" name="email" value="" id="email" class="form-control">
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="phone"><?php echo lang('create_user_phone_label', 'phone'); ?></label>
                              <div class="col-md-9 input-group">
                                    <input type="text" name="phone" value="" id="phone" class="form-control">
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="alamat">Alamat</label>
                              <div class="col-md-9 input-group">
                                    <input type="text" name="alamat" value="" id="alamat" class="form-control">
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="password"><?php echo lang('create_user_password_label', 'phone'); ?></label>
                              <div class="col-md-9 input-group">
                                    <input type="password" name="password" value="" id="password" class="form-control" required>
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="password_confirm"><?php echo lang('create_user_password_confirm_label', 'password_confirm'); ?></label>
                              <div class="col-md-9 input-group">
                                    <input type="password" name="password_confirm" value="" id="password_confirm" class="form-control" required>
                              </div>
                        </div>
                        <div class="modal-footer">
                              <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                        </div>
                  </div>
            </div>
      </div>
</div>


<?php echo form_close(); ?>