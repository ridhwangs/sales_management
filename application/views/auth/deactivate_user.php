<div class="row">
  <div class="col-md-8 container">
    <div class="tile">
      <div class="tile-title-w-btn">
        <h3><?php echo lang('deactivate_heading'); ?></h3>
        <a href="<?= site_url('auth') ?>" class="btn btn-sm btn-danger">[x] Tutup</a>
      </div>
      <hr>
      <div class="tile-body">
        <p><?php echo sprintf(lang('deactivate_subheading'), $user->username); ?></p>

        <?php echo form_open("auth/deactivate/" . $user->id); ?>
        <div class="form-group row">
          <div class="col-md-12 input-group">

            <?php echo lang('deactivate_confirm_y_label', 'confirm'); ?>
            <input type="radio" name="confirm" value="yes" checked="checked" />
            <?php echo lang('deactivate_confirm_n_label', 'confirm'); ?>
            <input type="radio" name="confirm" value="no" />
          </div>

        </div>
        <?php echo form_hidden($csrf); ?>
        <?php echo form_hidden(['id' => $user->id]); ?>

        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-block">Simpan</button>
        </div>

        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>