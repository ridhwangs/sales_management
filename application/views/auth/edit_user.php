<div id="infoMessage"><?php echo $message; ?></div>

<?php echo form_open(uri_string()); ?>

<div class="row">
      <div class="col-md-8 container">
            <div class="tile">
                  <div class="tile-title-w-btn">
                        <h3><?php echo lang('edit_user_heading'); ?></h3>
                        <a href="<?= site_url('auth') ?>" class="btn btn-sm btn-danger">[x] Tutup</a>
                  </div>
                  <div id="infoMessage"><?php echo $message; ?></div>
                  <hr>
                  <?php echo form_open(uri_string()); ?>
                  <div class="tile-body">
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="role">Role</label>
                              <div class="col-md-9 input-group">
                                    <select name="role" class="form-control" id="role" required>
                                          <option value="" selected disabled>--Silahkan pilih:</option>
                                          <option value="supervisor">Supervisor</option>
                                          <option value="marketing">Marketing</option>
                                    </select>
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="first_name"><?php echo lang('edit_user_fname_label', 'first_name'); ?></label>
                              <div class="col-md-9 input-group">
                                    <?php echo form_input($first_name); ?>
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="last_name"><?php echo lang('edit_user_lname_label', 'last_name'); ?></label>
                              <div class="col-md-9 input-group">
                                    <?php echo form_input($last_name); ?>
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="company"><?php echo lang('edit_user_company_label', 'company'); ?></label>
                              <div class="col-md-9 input-group">
                                    <?php echo form_input($company); ?>
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="phone"><?php echo lang('edit_user_phone_label', 'phone'); ?></label>
                              <div class="col-md-9 input-group">
                                    <?php echo form_input($phone); ?>
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="password"><?php echo lang('edit_user_password_label', 'password'); ?></label>
                              <div class="col-md-9 input-group">
                                    <?php echo form_input($password); ?>
                              </div>
                        </div>
                        <div class="form-group row">
                              <label class="control-label col-md-3 float-right" for="password_confirm"><?php echo lang('edit_user_password_confirm_label', 'password_confirm'); ?></label>
                              <div class="col-md-9 input-group">
                                    <?php echo form_input($password_confirm); ?>
                              </div>
                        </div>
                        <div class="modal-footer">
                              <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                        </div>
                  </div>
            </div>
      </div>
</div>



<?php echo form_hidden('id', $user->id); ?>
<?php echo form_hidden($csrf); ?>


<?php echo form_close(); ?>

<script>
      var role = "<?= $role; ?>";
      $("#role").val(role);
</script>