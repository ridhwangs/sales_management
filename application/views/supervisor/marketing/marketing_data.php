<div class="app-title">
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-mmksi.png'); ?>" alt="logo-mmksi" width="50px">
    </div>
    <div class="text-center">
        <h1> <?= $page_header; ?></h1>
        <p>Sales Management System</p>
    </div>
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-fuso.png'); ?>" alt="logo-fuso" width="50px">
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-title-w-btn">
                    <h5>Data Marketing<h5>
                            <a href="<?= site_url('auth') ?>">Management User</a>
                </div>
                <div class="tile-body">
                    <table class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th>No ID</th>
                                <th>NAMA LENGKAP</th>
                                <th>ALAMAT</th>
                                <th>No Handphone</th>
                                <th>Email</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!--Fetch data dari database-->
                            <?php foreach ($data->result() as $row) : ?>
                                <tr>
                                    <td><?php echo $row->no_identitas; ?></td>
                                    <td><?php echo $row->nm_lengkap; ?></td>
                                    <td><?php echo $row->alamat; ?></td>
                                    <td><?php echo $row->no_handphone; ?></td>
                                    <td><?php echo $row->email; ?></td>
                                    <td><a href="<?= site_url('supervisor/marketing/form/' . $row->id_marketing); ?>"><i class="fas fa-user-edit"></i></a></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="tile-footer">
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>