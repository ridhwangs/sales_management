<div class="app-title">
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-mmksi.png'); ?>" alt="logo-mmksi" width="50px">
    </div>
    <div class="text-center">
        <h1> <?= $page_header; ?></h1>
        <p>Sales Management System</p>
    </div>
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-fuso.png'); ?>" alt="logo-fuso" width="50px">
    </div>
</div>
<form method="POST" id="form-marketing" action="<?= site_url('supervisor/marketing/update/marketing'); ?>" autocomplete="off">
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-title-w-btn">
                        <h5>Form Marketing<h5>
                    </div>
                    <div class="tile-body">
                        <input type="hidden" name="id_marketing" id="id_marketing" value="<?= $dt_marketing->id_marketing; ?>">
                        <input type="hidden" name="id_users" id="id_users" value="<?= $users->id; ?>">
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="username">Username</label>
                            <div class="col-md-9 input-group">
                                <input type="text" name="username" class="form-control form-control-sm lowercase" id="username" placeholder="Username" value="<?= $users->username; ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="no_identitas">No Identitas</label>
                            <div class="col-md-9 input-group">
                                <input type="text" name="no_identitas" class="form-control form-control-sm" id="no_identitas" value="<?= $dt_marketing->no_identitas; ?>" placeholder="No Identitas">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="first_name">First Name</label>
                            <div class="col-md-9 input-group">
                                <input type="text" name="first_name" class="form-control form-control-sm" id="first_name" placeholder="First Name (*)" autofocus required value="<?= $users->first_name; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="first_name">Last Name</label>
                            <div class="col-md-9 input-group">
                                <input type="text" name="last_name" class="form-control form-control-sm" id="last_name" placeholder="Last Name (*)" value="<?= $users->last_name; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="no_handphone">No Handphone</label>
                            <div class="col-md-9 input-group">
                                <input type="text" name="no_handphone" class="form-control form-control-sm" id="no_handphone" placeholder="No Handphone (*)" value="<?= $users->phone; ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 float-right small" for="email">Email</label>
                            <div class="col-md-9 input-group">
                                <input type="email" name="email" class="form-control form-control-sm" id="email" placeholder="Email (*)" value="<?= $users->email; ?>" required readonly>
                            </div>
                        </div>
                        <div class="form-group-textarea row">
                            <label class="control-label col-md-3 float-right small" for="alamat">Alamat</label>
                            <div class="col-md-9 input-group">
                                <textarea name="alamat" class="form-control form-control-sm" id="alamat" placeholder="Alamat"><?= $dt_marketing->alamat; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary btn-block" id="btn-simpan" form="form-marketing">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>