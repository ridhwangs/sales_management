<div class="app-title">
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-mmksi.png'); ?>" alt="logo-mmksi" width="50px">
    </div>
    <div class="text-center">
        <h1> <?= $page_header; ?></h1>
        <p>Sales Management System</p>
    </div>
    <div class="tile-title">
        <img src="<?= base_url('assets/images/logo-fuso.png'); ?>" alt="logo-fuso" width="50px">
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-title-w-btn">
                    <h5>Kinerja Marketing<h5>
                </div>
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                                <tr class="text-center">
                                    <th>Nama Maketing</th>
                                    <th>Faktur Bobot (0.30)</th>
                                    <th>STNK Bobot (0.30)</th>
                                    <th>Karoseri Bobot (0.10)</th>
                                    <th>Total Faktur(0.30)</th>
                                    <th>Hasil Akhir</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Fetch data dari database-->
                                <?php
                                foreach ($data->result() as $rows => $row) :
                                    $ar_hari_faktur[$rows] = $row->hari_faktur;
                                    $ar_hari_stnk[$rows] = $row->hari_stnk;
                                    $ar_hari_karoseri[$rows] = $row->hari_karoseri;
                                    $ar_count_faktur[$rows] = $row->count_faktur;
                                ?>
                                    <tr class="uppercase">
                                        <td><?php echo $row->nm_marketing; ?></td>
                                        <td align="center"><?php echo $row->hari_faktur; ?> (<?= round($this->session->flashdata('min_hari_faktur') / $row->hari_faktur, 2); ?>)</td>
                                        <td align="center"><?php echo $row->hari_stnk; ?> (<?= round($this->session->flashdata('min_hari_stnk') / $row->hari_stnk, 2); ?>)</td>
                                        <td align="center"><?php echo $row->hari_karoseri; ?> (<?= round($this->session->flashdata('min_hari_karoseri') / $row->hari_karoseri, 2); ?>)</td>
                                        <td align="center"><?php echo $row->count_faktur; ?> (<?= round($row->count_faktur / $this->session->flashdata('max_count_faktur'), 2); ?>)</td>
                                        <td align="center"><?= round((0.30 * $this->session->flashdata('min_hari_faktur') / $row->hari_faktur) + (0.30 * $this->session->flashdata('min_hari_stnk') / $row->hari_stnk) + (0.10 * $this->session->flashdata('min_hari_karoseri') / $row->hari_karoseri) + (0.30 * $row->count_faktur / $this->session->flashdata('max_count_faktur')), 2); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                <?php
                                $this->session->set_flashdata('min_hari_faktur', min(array_filter($ar_hari_faktur)));
                                $this->session->set_flashdata('min_hari_stnk', min(array_filter($ar_hari_stnk)));
                                $this->session->set_flashdata('min_hari_karoseri', min(array_filter($ar_hari_karoseri)));
                                $this->session->set_flashdata('max_count_faktur', max(array_filter($ar_count_faktur)));
                                ?>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td align="center"><?= min(array_filter($ar_hari_faktur)); ?></td>
                                    <td align="center"><?= min(array_filter($ar_hari_stnk)); ?></td>
                                    <td align="center"><?= min(array_filter($ar_hari_karoseri)); ?></td>
                                    <td align="center"><?= max(array_filter($ar_count_faktur)); ?></td>
                                </tr>
                            </tfoot>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tile-footer">
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    <?php
    if (empty($this->input->get('refresh'))) {
        echo "$('#cover-spin').show(0);";
        header("refresh:3; url=" . site_url('supervisor/kinerja/marketing?refresh=yes'));
    }
    ?>
</script>