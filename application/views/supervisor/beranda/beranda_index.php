<div class="app-title fadeInDown">
  <div>
    <h1><i class="fa fa-dashboard"></i> <?= $page_header; ?></h1>
  </div>
</div>
<style>
  .carousel-control-prev-icon {
    background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23f00' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E");
  }

  .carousel-control-next-icon {
    background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23f00' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E");
  }
</style>

<div class="row">
  <div class="col-md-6">
    <div class="tile">
      <h3 class="tile-title">Monthly Sales</h3>
      <div class="embed-responsive embed-responsive-16by9">
        <canvas class="embed-responsive-item" id="lineChartDemo" width="519" height="292" style="width: 619px; height: 292px;"></canvas>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="tile">
      <div id="carouselExampleIndicators" class="carousel slide" style="width:100%; height: auto !important;" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <?php for ($i = 2; $i < 20; $i++) : ?>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <?php endfor; ?>

        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="container">
              <img src="<?= base_url('assets/images/product/1.jpeg') ?>" class="d-block w-100" alt="...">
            </div>
          </div>
          <?php for ($i = 2; $i < 20; $i++) : ?>
            <div class="carousel-item">
              <div class="container">
                <img src="<?= base_url('assets/images/product/' . $i . '.jpeg') ?>" class="d-block w-100" alt="...">
              </div>
            </div>
          <?php endfor; ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
</div>