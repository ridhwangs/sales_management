<div class="app-title">
    <div class="text-left">
        <h1> SPK Form</h1>
        <p>Formulir pengajuan SPK</p>
    </div>
    <div class="tile-title">
        <a href="<?= site_url('supervisor/spk/data') ?>" class="btn btn-sm btn-danger">Kembali [X]</a>
    </div>
</div>
<?php
if ($data_spk->status != 'approve') :
?>
    <div class="row" style="padding-bottom: 15px;">

        <div class="col-md-6">
            <a href="<?= site_url('supervisor/spk/update/approve/' . $this->uri->segment(5)); ?>" class="btn btn-success btn-block text-white">Approve</a>
        </div>
        <div class="col-md-6">
            <a href="<?= site_url('supervisor/spk/update/reject/' . $this->uri->segment(5)); ?>" class="btn btn-danger btn-block text-white">Reject</a>
        </div>
    </div>
<?php endif; ?>

<div class="row">

    <div class="col-md-6 readonly">
        <div class="tile">
            <div class="tile-title-w-btn">
                <h5>Form Edit SPK<h5>
            </div>
            <div class="tile-body">
                <input type="hidden" name="id_spk" id="id_spk">
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="no_spk">No SPK</label>
                    <div class="col-md-9 input-group">
                        <input type="number" name="no_spk" class="form-control form-control-sm" id="no_spk" placeholder="No SPK" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="tanggal">Tanggal</label>
                    <div class="col-md-9 input-group">
                        <input type="date" name="tanggal" class="form-control form-control-sm" id="tanggal" placeholder="" required>
                    </div>
                </div>
                <div class="form-group-textarea row">
                    <label class="control-label col-md-3 float-right small" for="sumber_informasi">Sumber Informasi</label>
                    <div class="col-md-9 input-group">
                        <textarea name="sumber_informasi" class="form-control form-control-sm" id="sumber_informasi" placeholder="Sumber Informasi"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="tipe_kedatangan">Tipe Kedatangan</label>
                    <div class="col-md-9 input-group">
                        <input type="text" name="tipe_kedatangan" class="form-control form-control-sm" id="tipe_kedatangan" placeholder="Tipe Kedatangan">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="merk_kendaraan_saat_ini">Merk Kendaraan Saat Ini</label>
                    <div class="col-md-9 input-group">
                        <input type="text" name="merk_kendaraan_saat_ini" class="form-control form-control-sm" id="merk_kendaraan_saat_ini" placeholder="Merk Kendaraan Saat Ini">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="tipe_identitas_stnk">Tipe Identitas STNK</label>
                    <div class="col-md-9 input-group">
                        <input type="text" name="tipe_identitas_stnk" class="form-control form-control-sm" id="tipe_identitas_stnk" placeholder="Tipe Identitas STNK">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="no_identitas_stnk">No Identitas STNK</label>
                    <div class="col-md-9 input-group">
                        <input type="text" name="no_identitas_stnk" class="form-control form-control-sm" id="no_identitas_stnk" placeholder="No Identitas STNK">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="nm_stnk">Nama STNK</label>
                    <div class="col-md-9 input-group">
                        <input type="text" name="nm_stnk" class="form-control form-control-sm" id="nm_stnk" placeholder="Nama STNK">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="industri">Industri</label>
                    <div class="col-md-9 input-group">
                        <input type="text" name="industri" class="form-control form-control-sm" id="industri" placeholder="Industri">
                    </div>
                </div>
                <div class="form-group-textarea row">
                    <label class="control-label col-md-3 float-right small" for="catatan">Catatan</label>
                    <div class="col-md-9 input-group">
                        <textarea name="catatan" class="form-control form-control-sm" id="catatan" placeholder="Catatan"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 readonly">
        <div class="tile">
            <div class="tile-title-w-btn">
                <h5>Data Konsumen<h5>
            </div>
            <div class="tile-body">
                <input type="hidden" name="id" id="id">
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="jenis">Jenis</label>
                    <div class="col-md-9 input-group">
                        <select name="jenis" class="form-control form-control-sm" id="jenis" required>
                            <option value="" selected disabled>Silahkan Pilih</option>
                            <option value="perusahaan">1. Perusahaan</option>
                            <option value="perorangan">2. Perorangan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="nm_lengkap">Nama Lengkap</label>
                    <div class="col-md-9 input-group">
                        <input type="text" name="nm_lengkap" class="form-control form-control-sm" id="nm_lengkap" placeholder="Nama Lengkap (*)" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="no_handphone">No Handphone</label>
                    <div class="col-md-9 input-group">
                        <input type="text" name="no_handphone" class="form-control form-control-sm" id="no_handphone" placeholder="No Handphone (*)" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="no_telp">No Telp</label>
                    <div class="col-md-9 input-group">
                        <input type="text" name="no_telp" class="form-control form-control-sm" id="no_telp" placeholder="No Telp">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="email">Email</label>
                    <div class="col-md-9 input-group">
                        <input type="email" name="email" class="form-control form-control-sm" id="email" placeholder="Email">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="jenis_kelamin">Jenis Kelamin</label>
                    <div class="col-md-9 input-group">
                        <select name="jenis_kelamin" class="form-control form-control-sm" id="jenis_kelamin" required>
                            <option value="" selected disabled>Silahkan Pilih</option>
                            <option value="p">1. Pria</option>
                            <option value="w">2. Wanita</option>
                        </select>
                    </div>
                </div>
                <div class="form-group-textarea row">
                    <label class="control-label col-md-3 float-right small" for="alamat">Alamat</label>
                    <div class="col-md-9 input-group">
                        <textarea name="alamat" class="form-control form-control-sm" id="alamat" placeholder="Alamat"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="kota">Kota</label>
                    <div class="col-md-9 input-group">
                        <input type="text" name="kota" class="form-control form-control-sm" id="kota" placeholder="Kota">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="provinsi">Provinsi</label>
                    <div class="col-md-9 input-group">
                        <input type="text" name="provinsi" class="form-control form-control-sm" id="provinsi" placeholder="Provinsi">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="kode_pos">Kode Pos</label>
                    <div class="col-md-9 input-group">
                        <input type="text" name="kode_pos" class="form-control form-control-sm" id="kode_pos" placeholder="Kode Pos">
                    </div>
                </div>
                <div class="form-group-textarea row">
                    <label class="control-label col-md-3 float-right small" for="topik">Topik</label>
                    <div class="col-md-9 input-group">
                        <textarea name="topik" class="form-control form-control-sm" id="topik" placeholder="Topik"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 float-right small" for="status">Status</label>
                    <div class="col-md-9 input-group">
                        <select name="status" class="form-control form-control-sm" id="status" required>
                            <option value="" selected disabled>Silahkan Pilih</option>
                            <option value="prospek">1. Prospek</option>
                            <option value="suspek">2. Suspek</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function() {
        $('.readonly').find('input, textarea, select').attr('disabled', 'disabled');
        $("#jenis").val('<?= $data_konsumen->jenis; ?>');
        $("#nm_lengkap").val('<?= $data_konsumen->nm_lengkap; ?>');
        $("#no_handphone").val('<?= $data_konsumen->no_handphone; ?>');
        $("#no_telp").val('<?= $data_konsumen->no_telp; ?>');
        $("#email").val('<?= $data_konsumen->email; ?>');
        $("#jenis_kelamin").val('<?= $data_konsumen->jenis_kelamin; ?>');
        $("#alamat").val('<?= $data_konsumen->alamat; ?>');
        $("#kota").val('<?= $data_konsumen->kota; ?>');
        $("#provinsi").val('<?= $data_konsumen->provinsi; ?>');
        $("#kode_pos").val('<?= $data_konsumen->kode_pos; ?>');
        $("#topik").val('<?= $data_konsumen->topik; ?>');
        $("#status").val('<?= $data_konsumen->status; ?>');

        $("#id_spk").val('<?= $data_spk->id_spk; ?>');
        $("#no_spk").val('<?= $data_spk->no_spk; ?>');
        $("#tanggal").val('<?= $data_spk->tanggal; ?>');
        $("#sumber_informasi").val('<?= $data_spk->sumber_informasi; ?>');
        $("#tipe_kedatangan").val('<?= $data_spk->tipe_kedatangan; ?>');
        $("#merk_kendaraan_saat_ini").val('<?= $data_spk->merk_kendaraan_saat_ini; ?>');
        $("#tipe_identitas_stnk").val('<?= $data_spk->tipe_identitas_stnk; ?>');
        $("#no_identitas_stnk").val('<?= $data_spk->no_identitas_stnk; ?>');
        $("#nm_stnk").val('<?= $data_spk->nm_stnk; ?>');
        $("#industri").val('<?= $data_spk->industri; ?>');
        $("#catatan").val('<?= $data_spk->catatan; ?>');
    });
</script>