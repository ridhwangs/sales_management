<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Faktur_model extends CI_Model
{

    public function read($where)
    {
        $this->db->select('
            dt_marketing.nm_lengkap AS nm_marketing,
            dt_marketing.alamat AS alamat_marketing,
            dt_marketing.no_handphone AS no_marketing,
            dt_marketing.email AS email_marketing,

            dt_konsumen.nm_lengkap AS nm_konsumen,
            dt_konsumen.alamat AS alamat_konsumen,
            dt_konsumen.no_handphone AS no_konsumen,
            dt_konsumen.email AS email_konsumen,

            proses_kendaraan.tgl_faktur AS tgl_faktur,
            proses_kendaraan.tgl_estimasi_selesai AS tgl_estimasi_selesai,

            mst_spk.no_spk AS no_spk,
            dt_kendaraan.merk AS merk,
            dt_kendaraan.tipe AS tipe,
            dt_kendaraan.warna AS warna,
            dt_kendaraan.harga_dasar AS harga_dasar,
            dt_kendaraan.diskon AS diskon,
            dt_kendaraan.harga_estimasi AS harga_estimasi,
            dt_kendaraan.no_rangka AS no_rangka,

            mst_karoseri.nm_karoseri AS nm_karoseri
        ');
        $this->db->from('proses_kendaraan');
        $this->db->join('dt_faktur', 'dt_faktur.id_faktur = proses_kendaraan.id_faktur', 'left');
        $this->db->join('mst_karoseri', 'mst_karoseri.kd_karoseri = dt_faktur.kd_karoseri', 'left');
        $this->db->join('dt_kendaraan', 'dt_kendaraan.id_kendaraan = dt_faktur.id_kendaraan', 'left');
        $this->db->join('dt_konsumen', 'dt_konsumen.id_konsumen = dt_faktur.id_konsumen', 'left');
        $this->db->join('mst_spk', 'mst_spk.id_spk = dt_kendaraan.id_spk', 'left');
        $this->db->join('dt_marketing', 'dt_marketing.id_marketing = mst_spk.id_marketing', 'left');
        $this->db->where('mst_spk.id_marketing', $where);
        $query = $this->db->get();
        return $query;
    }

    public function read_pagination($limit, $start, $where)
    {
        $this->db->select('
            dt_marketing.nm_lengkap AS nm_marketing,
            dt_marketing.alamat AS alamat_marketing,
            dt_marketing.no_handphone AS no_marketing,
            dt_marketing.email AS email_marketing,

            dt_faktur.tipe_rear_body AS tipe_rear_body,

            dt_konsumen.nm_lengkap AS nm_konsumen,
            dt_konsumen.alamat AS alamat_konsumen,
            dt_konsumen.no_handphone AS no_konsumen,
            dt_konsumen.email AS email_konsumen,
            dt_konsumen.id_konsumen AS id_konsumen,


            proses_kendaraan.id_faktur AS id_faktur,
            proses_kendaraan.tgl_faktur AS tgl_faktur,
            proses_kendaraan.tgl_estimasi_selesai AS tgl_estimasi_selesai,
            proses_kendaraan.tgl_selesai AS tgl_selesai,
            proses_kendaraan.id_proses AS id_proses,
            proses_kendaraan.tgl_stnk AS tgl_stnk,
            proses_kendaraan.tgl_estimasi_stnk AS tgl_estimasi_stnk,
            proses_kendaraan.tgl_karoseri AS tgl_karoseri,
            proses_kendaraan.tgl_estimasi_karoseri AS tgl_estimasi_karoseri,
            proses_kendaraan.hari_faktur AS hari_faktur,
            proses_kendaraan.hari_stnk AS hari_stnk,
            proses_kendaraan.hari_karoseri AS hari_karoseri,

            mst_spk.no_spk AS no_spk,
            mst_spk.tanggal AS tanggal,
            mst_spk.sumber_informasi AS sumber_informasi,
            mst_spk.tipe_kedatangan AS tipe_kedatangan,
            mst_spk.merk_kendaraan_saat_ini AS merk_kendaraan_saat_ini,
            mst_spk.tipe_identitas_stnk AS tipe_identitas_stnk,
            mst_spk.no_identitas_stnk AS no_identitas_stnk,
            mst_spk.nm_stnk AS nm_stnk,
            mst_spk.industri AS industri,
            mst_spk.catatan AS catatan,
            mst_spk.status AS status,
            mst_spk.id_spk AS id_spk,

            dt_kendaraan.merk AS merk,
            dt_kendaraan.tipe AS tipe,
            dt_kendaraan.warna AS warna,
            dt_kendaraan.harga_dasar AS harga_dasar,
            dt_kendaraan.diskon AS diskon,
            dt_kendaraan.harga_estimasi AS harga_estimasi,
            dt_kendaraan.no_rangka AS no_rangka,

            mst_karoseri.kd_karoseri AS kd_karoseri,
            mst_karoseri.nm_karoseri AS nm_karoseri
        ');
        $this->db->from('proses_kendaraan');
        $this->db->join('dt_faktur', 'dt_faktur.id_faktur = proses_kendaraan.id_faktur', 'left');
        $this->db->join('dt_kendaraan', 'dt_kendaraan.id_kendaraan = dt_faktur.id_kendaraan', 'left');
        $this->db->join('dt_konsumen', 'dt_konsumen.id_konsumen = dt_faktur.id_konsumen', 'left');
        $this->db->join('mst_spk', 'mst_spk.id_spk = dt_kendaraan.id_spk', 'left');
        $this->db->join('dt_marketing', 'dt_marketing.id_marketing = mst_spk.id_marketing', 'left');
        $this->db->join('mst_karoseri', 'mst_karoseri.kd_karoseri = dt_faktur.kd_karoseri', 'left');
        $this->db->where('mst_spk.id_marketing', $where);
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query;
    }
}
