<?php
defined('BASEPATH') or exit('No direct script access allowed');

class View_model extends CI_Model
{

    public function read($where)
    {
        $this->db->select('
            dt_marketing.nm_lengkap AS nm_marketing,
            dt_marketing.alamat AS alamat_marketing,
            dt_marketing.no_handphone AS no_marketing,
            dt_marketing.email AS email_marketing,

            dt_konsumen.nm_lengkap AS nm_konsumen,
            dt_konsumen.alamat AS alamat_konsumen,
            dt_konsumen.no_handphone AS no_konsumen,
            dt_konsumen.email AS email_konsumen,

            proses_kendaraan.tgl_faktur AS tgl_faktur,
            proses_kendaraan.tgl_estimasi_selesai AS tgl_estimasi_selesai,
            proses_kendaraan.tgl_selesai AS tgl_selesai,

            mst_spk.no_spk AS no_spk,
            dt_kendaraan.merk AS merk,
            dt_kendaraan.tipe AS tipe,
            dt_kendaraan.warna AS warna,
            dt_kendaraan.harga_dasar AS harga_dasar,
            dt_kendaraan.diskon AS diskon,
            dt_kendaraan.harga_estimasi AS harga_estimasi,
            dt_kendaraan.no_rangka AS no_rangka,

            mst_karoseri.nm_karoseri AS nm_karoseri,
            dt_faktur.tipe_rear_body AS tipe_rear_body,
        ');
        $this->db->from('proses_kendaraan');
        $this->db->join('dt_faktur', 'dt_faktur.id_faktur = proses_kendaraan.id_faktur', 'left');
        $this->db->join('dt_kendaraan', 'dt_kendaraan.id_kendaraan = dt_faktur.id_kendaraan', 'left');
        $this->db->join('dt_konsumen', 'dt_konsumen.id_konsumen = dt_faktur.id_konsumen', 'left');
        $this->db->join('mst_spk', 'mst_spk.id_spk = dt_kendaraan.id_spk', 'left');
        $this->db->join('dt_marketing', 'dt_marketing.id_marketing = mst_spk.id_marketing', 'left');
        $this->db->join('mst_karoseri', 'mst_karoseri.kd_karoseri = dt_faktur.kd_karoseri', 'left');
        $this->db->where('id_proses', $where);
        $query = $this->db->get();
        return $query;
    }
}
