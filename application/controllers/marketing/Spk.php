<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Spk extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   * 	- or -
   * 		http://example.com/index.php/welcome/index
   * 	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */

  public function __construct()
  {
    parent::__construct();
    if (!$this->ion_auth->logged_in()) {
      redirect('auth/login', 'refresh');
    }
    if ($this->session->role != 'marketing') {
      redirect('auth/logout', 'refresh');
    }
    $this->_init();
  }

  private function _init()
  {
    $this->output->set_template('marketing_layout');
  }

  public function form($id)
  {
    $data = array(
      'page_header' => 'Form SPK',
      'data_konsumen' => $this->crud_model->read('dt_konsumen', ['id_konsumen' => $id])->row(),
    );
    $this->load->view('marketing/spk/spk_form', $data);
  }

  public function form_edit($id_spk)
  {
    $mst_spk = $this->crud_model->read('mst_spk', ['id_spk' => $id_spk])->row();
    $data = array(
      'page_header' => 'Form Edit SPK',
      'data_konsumen' => $this->crud_model->read('dt_konsumen', ['id_konsumen' => $mst_spk->id_konsumen])->row(),
      'data_spk' => $this->crud_model->read('mst_spk', ['id_spk' => $id_spk])->row()
    );
    $this->load->view('marketing/spk/spk_form_edit', $data);
  }

  public function data()
  {
    //konfigurasi pagination
    $config['base_url'] = site_url('supervisor/marketing/data'); //site url
    $config['total_rows'] = $this->crud_model->read('mst_spk', ['id_marketing' => $this->session->id_marketing])->num_rows(); //total row
    $config['per_page'] = 10;  //show record per halaman
    $config["uri_segment"] = 4;  // uri parameter
    $choice = $config["total_rows"] / $config["per_page"];
    $config["num_links"] = floor($choice);

    // Membuat Style pagination untuk BootStrap v4
    $config['first_link']       = 'First';
    $config['last_link']        = 'Last';
    $config['next_link']        = 'Next';
    $config['prev_link']        = 'Prev';
    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
    $config['full_tag_close']   = '</ul></nav></div>';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']    = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['prev_tagl_close']  = '</span>Next</li>';
    $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
    $config['first_tagl_close'] = '</span></li>';
    $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tagl_close']  = '</span></li>';

    $this->pagination->initialize($config);
    $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

    //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
    $data['data'] = $this->crud_model->read_pagination('mst_spk', $config["per_page"], $data['page'], ['id_marketing' => $this->session->id_marketing]);

    $data['pagination'] = $this->pagination->create_links();
    $data['page_header'] = 'Data SPK';

    $this->load->view('marketing/spk/spk_data', $data);
  }

  public function create($attr)
  {
    switch ($attr) {
      case 'spk':
        $mkt_data = $this->crud_model->read('dt_marketing', ['id_marketing' => $this->session->id_marketing])->row();
        $where = [
          'no_spk' => $this->input->post('no_spk'),
        ];
        $data = [
          'id_supervisor' => $mkt_data->id_supervisor,
          'id_marketing' => $mkt_data->id_marketing,
          'id_konsumen' => $this->input->post('id_konsumen'),
          'no_spk' => $this->input->post('no_spk'),
          'tanggal' => $this->input->post('tanggal'),
          'sumber_informasi' => $this->input->post('sumber_informasi'),
          'tipe_kedatangan' => $this->input->post('tipe_kedatangan'),
          'merk_kendaraan_saat_ini' => $this->input->post('merk_kendaraan_saat_ini'),
          'tipe_identitas_stnk' => $this->input->post('tipe_identitas_stnk'),
          'no_identitas_stnk' => $this->input->post('no_identitas_stnk'),
          'nm_stnk' => $this->input->post('nm_stnk'),
          'industri' => $this->input->post('industri'),
          'catatan' => $this->input->post('catatan'),
          'status' => 'open'
        ];
        $num = $this->crud_model->read('mst_spk', $where)->num_rows();
        if ($num > 0) {
          $this->session->set_flashdata('message', "No SPK sudah terdaftar");
        } else {
          $this->session->set_flashdata('message', "Data SPK Berhasil di Simpan");
          $this->crud_model->create('mst_spk', $data);
        }
        redirect('marketing/spk/data');
        die();
        break;

      default:
        show_404();
        break;
    }
  }

  public function update($attr)
  {
    switch ($attr) {
      case 'spk':
        $where = [
          'id_spk' => $this->input->post('id_spk')
        ];
        $data = [
          'no_spk' => $this->input->post('no_spk'),
          'tanggal' => $this->input->post('tanggal'),
          'sumber_informasi' => $this->input->post('sumber_informasi'),
          'tipe_kedatangan' => $this->input->post('tipe_kedatangan'),
          'merk_kendaraan_saat_ini' => $this->input->post('merk_kendaraan_saat_ini'),
          'tipe_identitas_stnk' => $this->input->post('tipe_identitas_stnk'),
          'no_identitas_stnk' => $this->input->post('no_identitas_stnk'),
          'nm_stnk' => $this->input->post('nm_stnk'),
          'industri' => $this->input->post('industri'),
          'catatan' => $this->input->post('catatan'),
        ];
        $this->crud_model->update('mst_spk', $where, $data);
        $this->session->set_flashdata('message', "Data SPK Berhasil di Simpan");
        redirect($this->agent->referrer());
        die();
        break;

      default:
        show_404();
        break;
    }
  }

  public function delete($attr, $id = null)
  {
    switch ($attr) {
      case 'spk':
        $where = [
          'id_spk' => $id
        ];
        $this->session->set_flashdata('message', "Data SPK Berhasil di Hapus");
        $this->crud_model->delete('mst_spk', $where);
        redirect($this->agent->referrer());
        die();
        break;

      default:
        # code...
        break;
    }
  }
}
