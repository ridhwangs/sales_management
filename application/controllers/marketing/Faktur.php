<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Faktur extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   * 	- or -
   * 		http://example.com/index.php/welcome/index
   * 	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */

  public function __construct()
  {
    parent::__construct();
    if (!$this->ion_auth->logged_in()) {
      redirect('auth/login', 'refresh');
    }
    if ($this->session->role != 'marketing') {
      redirect('auth/logout', 'refresh');
    }
    $this->_init();
    $this->load->model(['view_model', 'faktur_model']);
  }

  private function _init()
  {
    $this->output->set_template('marketing_layout');
  }

  public function kendaraan($id_spk)
  {
    $query = $this->crud_model->read('dt_kendaraan', ['id_spk' => $id_spk]);
    if ($query->num_rows() > 0) {
      $data = $query->row();
      redirect('marketing/faktur/karoseri/' . $data->id_kendaraan);
      die();
    }
    $data = array(
      'page_header' => 'Proses pengajuan Faktur Kendaraan Step-1',
    );
    $this->load->view('marketing/faktur/faktur_kendaraan', $data);
  }

  public function karoseri($id_kendaraan)
  {
    $query = $this->crud_model->read('dt_faktur', ['id_kendaraan' => $id_kendaraan]);
    if ($query->num_rows() > 0) {
      $data = $query->row();
      redirect('marketing/faktur/proses/' . $data->id_faktur);
      die();
    }
    $dt_kendaraan = $this->crud_model->read('dt_kendaraan', ['id_kendaraan' => $id_kendaraan])->row();
    $data = array(
      'page_header' => 'Proses pengajuan Faktur Kendaraan Step-2',
      'data_kendaraan' => $this->crud_model->read('dt_kendaraan', ['id_kendaraan' => $id_kendaraan])->row(),
      'kd_karoseri' => $this->crud_model->read('mst_karoseri')->result(),
      'id_kendaraan' => $id_kendaraan,
    );
    $this->load->view('marketing/faktur/faktur_karoseri', $data);
  }

  public function proses($id_faktur)
  {
    $query = $this->crud_model->read('proses_kendaraan', ['id_faktur' => $id_faktur]);
    if ($query->num_rows() > 0) {
      $data = $query->row();
      redirect('marketing/faktur/view/' . $data->id_proses);
      die();
    }
    $data_faktur = $this->crud_model->read('dt_faktur', ['id_faktur' => $id_faktur])->row();
    $data = array(
      'page_header' => 'Proses pengajuan Faktur Kendaraan Step-3',
      'data_kendaraan' => $this->crud_model->read('dt_kendaraan', ['id_kendaraan' => $data_faktur->id_kendaraan])->row(),
      'data_faktur' => $this->crud_model->read('dt_faktur', ['id_faktur' => $id_faktur])->row(),
      'data_karoseri' => $this->crud_model->read('mst_karoseri', ['kd_karoseri' => $data_faktur->kd_karoseri])->row(),
      'kd_karoseri' => $this->crud_model->read('mst_karoseri')->result(),
      'id_faktur' => $data_faktur->id_faktur,
    );
    $this->load->view('marketing/faktur/faktur_proses', $data);
  }

  public function view($id_proses)
  {
    $data = array(
      'page_header' => 'Faktur Kendaraan',
      'data' => $this->view_model->read($id_proses)->row(),
    );
    $this->load->view('marketing/faktur/faktur_view', $data);
  }

  public function faktur_kendaraan()
  {
    //konfigurasi pagination
    $config['base_url'] = site_url('marketing/faktur/faktur_kendaraan'); //site url
    $config['total_rows'] = $this->faktur_model->read($this->session->id_marketing)->num_rows(); //total row
    $config['per_page'] = 10;  //show record per halaman
    $config["uri_segment"] = 4;  // uri parameter
    $choice = $config["total_rows"] / $config["per_page"];
    $config["num_links"] = floor($choice);

    // Membuat Style pagination untuk BootStrap v4
    $config['first_link']       = 'First';
    $config['last_link']        = 'Last';
    $config['next_link']        = 'Next';
    $config['prev_link']        = 'Prev';
    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
    $config['full_tag_close']   = '</ul></nav></div>';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']    = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['prev_tagl_close']  = '</span>Next</li>';
    $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
    $config['first_tagl_close'] = '</span></li>';
    $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tagl_close']  = '</span></li>';

    $this->pagination->initialize($config);
    $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

    //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
    $data['data'] = $this->faktur_model->read_pagination($config["per_page"], $data['page'], $this->session->id_marketing);

    $data['pagination'] = $this->pagination->create_links();
    $data['page_header'] = 'Faktur Kendaraan';

    $this->load->view('marketing/faktur/faktur_kendaraan_data', $data);
  }

  public function create($attr)
  {
    switch ($attr) {
      case 'kendaraan':
        $where = [
          'no_rangka' => $this->input->post('no_rangka')
        ];
        $data = [
          'id_spk' => $this->input->post('id_spk'),
          'merk' => $this->input->post('merk'),
          'tipe' => $this->input->post('tipe'),
          'warna' => $this->input->post('warna'),
          'harga_dasar' => str_replace('.', '', $this->input->post('harga_dasar')),
          'diskon' => str_replace('.', '', $this->input->post('diskon')),
          'harga_estimasi' => str_replace('.', '', $this->input->post('harga_estimasi')),
          'no_rangka' => $this->input->post('no_rangka'),
          'created_by' => $this->session->username,
          'created_at' => date('Y-m-d H:i:s')
        ];

        $num = $this->crud_model->read('dt_kendaraan', $where)->num_rows();
        if ($num > 0) {
          $this->session->set_flashdata('message', "No. Rangka sudah terdaftar");
          redirect($this->agent->referrer());
        } else {
          $this->session->set_flashdata('message', "Step 1 Berhasil di Simpan");
          $this->crud_model->create('dt_kendaraan', $data);
          $last_id = $this->db->insert_id();
          redirect('marketing/faktur/karoseri/' . $last_id);
        }
        die();
        break;
      case 'karoseri':

        $dt_kendaraan = $this->crud_model->read('dt_kendaraan', ['id_kendaraan' => $this->input->post('id_kendaraan')])->row();
        $dt_spk = $this->crud_model->read('mst_spk', ['id_spk' => $dt_kendaraan->id_spk])->row();
        $data = [
          'id_konsumen' => $dt_spk->id_konsumen,
          'id_kendaraan' => $dt_kendaraan->id_kendaraan,
          'id_spk' => $dt_kendaraan->id_spk,
          'kd_karoseri' => $this->input->post('kd_karoseri'),
          'tipe_rear_body' => $this->input->post('tipe_rear_body'),
          'status' => 'open',
          'created_by' => $this->session->username,
          'created_at' => date('Y-m-d H:i:s')
        ];
        $this->session->set_flashdata('message', "Step 2 Berhasil di Simpan");
        $this->crud_model->create('dt_faktur', $data);
        $last_id = $this->db->insert_id();
        redirect('marketing/faktur/proses/' . $last_id);
        die();
        break;
      case 'proses':
        $id_faktur = $this->input->post('id_faktur');
        $dt_faktur = $this->crud_model->read('dt_faktur', ['id_faktur' => $id_faktur])->row();
        $data = [
          'id_faktur' => $id_faktur,
          'id_spk' => $dt_faktur->id_spk,
          'tgl_faktur' => $this->input->post('tgl_faktur'),
          'tgl_estimasi_selesai' => $this->input->post('tgl_estimasi_selesai'),
          'tgl_estimasi_stnk' => $this->input->post('tgl_estimasi_stnk'),
          'tgl_estimasi_karoseri' => $this->input->post('tgl_estimasi_karoseri'),
          'hari_karoseri' => '0',
        ];
        $this->session->set_flashdata('message', "Step 3 Berhasil di Simpan");
        $this->crud_model->create('proses_kendaraan', $data);
        $last_id = $this->db->insert_id();
        redirect('marketing/faktur/view/' . $last_id);
        break;
      default:
        show_404();
        break;
    }
  }

  public function update($attr, $id = null)
  {
    switch ($attr) {
      case 'selesai':
        $where = [
          'id_proses' => $id
        ];
        $proses_kendaraan = $this->crud_model->read('proses_kendaraan', $where)->row();
        $tgl1 = new DateTime($proses_kendaraan->tgl_estimasi_selesai);
        $tgl2 = new DateTime(date('Y-m-d'));
        $d = $tgl2->diff($tgl1)->days;
        if ($proses_kendaraan->tgl_estimasi_selesai > date('Y-m-d')) {
          $hari_faktur = -$d;
        } else {
          $hari_faktur = $d;
        }
        $data = [
          'tgl_selesai' => date('Y-m-d'),
          'hari_faktur' => $hari_faktur
        ];
        $this->crud_model->update('proses_kendaraan', $where, $data);
        $this->session->set_flashdata('message', "Faktur berhasil Selesai");
        redirect($this->agent->referrer());
        die();
        break;

      default:
        show_404();
        break;
    }
  }

  public function delete($attr, $id = null)
  {
    switch ($attr) {
      case 'karoseri':
        $where = [
          'id_faktur' => $id
        ];
        $dt_kendaraan = $this->crud_model->read('dt_faktur', $where)->row();
        $this->crud_model->delete('dt_faktur', $where);
        $this->session->set_flashdata('message', "Berhasil di hapus");
        redirect('marketing/faktur/karoseri/' . $dt_kendaraan->id_kendaraan);
        die();
        break;
      case 'kendaraan':
        $where = [
          'id_kendaraan' => $id
        ];
        $dt_kendaraan = $this->crud_model->read('dt_kendaraan', $where)->row();
        $this->crud_model->delete('dt_kendaraan', $where);
        $this->session->set_flashdata('message', "Berhasil di hapus");
        redirect('marketing/faktur/kendaraan/' . $dt_kendaraan->id_spk);
        die();
        break;
      case 'faktur_kendaraan':
        $where = [
          'id_faktur' => $id
        ];
        $this->crud_model->delete('dt_faktur', $where);
        $this->session->set_flashdata('message', "Berhasil di hapus");
        redirect($this->agent->referrer());
        die();
        break;
      default:
        show_404();
        break;
    }
  }
}
