<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Mengelola extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   * 	- or -
   * 		http://example.com/index.php/welcome/index
   * 	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */

  public function __construct()
  {
    parent::__construct();
    if (!$this->ion_auth->logged_in()) {
      redirect('auth/login', 'refresh');
    }
    if ($this->session->role != 'marketing') {
      redirect('auth/logout', 'refresh');
    }
    $this->_init();
    $this->load->model(['faktur_model']);
  }

  private function _init()
  {
    $this->output->set_template('marketing_layout');
  }


  public function kendaraan()
  {
    //konfigurasi pagination
    $config['base_url'] = site_url('marketing/faktur/faktur_kendaraan'); //site url
    $config['total_rows'] = $this->faktur_model->read($this->session->id_marketing)->num_rows(); //total row
    $config['per_page'] = 10;  //show record per halaman
    $config["uri_segment"] = 4;  // uri parameter
    $choice = $config["total_rows"] / $config["per_page"];
    $config["num_links"] = floor($choice);

    // Membuat Style pagination untuk BootStrap v4
    $config['first_link']       = 'First';
    $config['last_link']        = 'Last';
    $config['next_link']        = 'Next';
    $config['prev_link']        = 'Prev';
    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
    $config['full_tag_close']   = '</ul></nav></div>';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']    = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['prev_tagl_close']  = '</span>Next</li>';
    $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
    $config['first_tagl_close'] = '</span></li>';
    $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tagl_close']  = '</span></li>';

    $this->pagination->initialize($config);
    $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

    //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
    $data['data'] = $this->faktur_model->read_pagination($config["per_page"], $data['page'], $this->session->id_marketing);

    $data['pagination'] = $this->pagination->create_links();
    $data['page_header'] = 'Mengelola Kendaraan';

    $this->load->view('marketing/mengelola/mengelola_kendaraan', $data);
  }

  public function update($attr, $id = null)
  {
    switch ($attr) {
      case 'selesai_stnk':
        $where = [
          'id_proses' => $id
        ];
        $proses_kendaraan = $this->crud_model->read('proses_kendaraan', $where)->row();
        $tgl1 = new DateTime($proses_kendaraan->tgl_estimasi_stnk);
        $tgl2 = new DateTime(date('Y-m-d'));
        $d = $tgl2->diff($tgl1)->days;
        if ($proses_kendaraan->tgl_estimasi_stnk > date('Y-m-d')) {
          $hari_stnk = -$d;
        } else {
          $hari_stnk = $d;
        }
        $data = [
          'tgl_stnk' => date('Y-m-d'),
          'hari_stnk' => $hari_stnk,
        ];
        $this->crud_model->update('proses_kendaraan', $where, $data);
        $this->session->set_flashdata('message', "Data berhasil di simpan");
        redirect($this->agent->referrer());
        die();
        break;
      case 'selesai_karoseri':
        $where = [
          'id_proses' => $id
        ];
        $proses_kendaraan = $this->crud_model->read('proses_kendaraan', $where)->row();
        $tgl1 = new DateTime($proses_kendaraan->tgl_estimasi_karoseri);
        $tgl2 = new DateTime(date('Y-m-d'));
        $d = $tgl2->diff($tgl1)->days;
        if ($proses_kendaraan->tgl_estimasi_karoseri > date('Y-m-d')) {
          $hari_karoseri = -$d;
        } else {
          $hari_karoseri = $d;
        }
        $data = [
          'tgl_karoseri' => date('Y-m-d'),
          'hari_karoseri' => $hari_karoseri
        ];
        $this->crud_model->update('proses_kendaraan', $where, $data);
        $this->session->set_flashdata('message', "Data berhasil di simpan");
        redirect($this->agent->referrer());
        die();
        break;
      default:
        show_404();
        break;
    }
  }
}
