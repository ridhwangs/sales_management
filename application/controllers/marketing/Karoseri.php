<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Karoseri extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   * 	- or -
   * 		http://example.com/index.php/welcome/index
   * 	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */

  public function __construct()
  {
    parent::__construct();
    if (!$this->ion_auth->logged_in()) {
      redirect('auth/login', 'refresh');
    }
    if ($this->session->role != 'marketing') {
      redirect('auth/logout', 'refresh');
    }
    $this->_init();
  }

  private function _init()
  {
    $this->output->set_template('marketing_layout');
  }

  public function baru()
  {
    $data = array(
      'page_header' => 'Daftar karoseri Baru',
    );
    $this->load->view('marketing/karoseri/karoseri_baru', $data);
  }

  public function data()
  {
    //konfigurasi pagination
    $config['base_url'] = site_url('marketing/karoseri/data'); //site url
    $config['total_rows'] = $this->crud_model->read('mst_karoseri')->num_rows(); //total row
    $config['per_page'] = 10;  //show record per halaman
    $config["uri_segment"] = 4;  // uri parameter
    $choice = $config["total_rows"] / $config["per_page"];
    $config["num_links"] = floor($choice);

    // Membuat Style pagination untuk BootStrap v4
    $config['first_link']       = 'First';
    $config['last_link']        = 'Last';
    $config['next_link']        = 'Next';
    $config['prev_link']        = 'Prev';
    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
    $config['full_tag_close']   = '</ul></nav></div>';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']    = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['prev_tagl_close']  = '</span>Next</li>';
    $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
    $config['first_tagl_close'] = '</span></li>';
    $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tagl_close']  = '</span></li>';

    $this->pagination->initialize($config);
    $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

    //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
    $data['data'] = $this->crud_model->read_pagination('mst_karoseri', $config["per_page"], $data['page']);

    $data['pagination'] = $this->pagination->create_links();
    $data['page_header'] = 'Data karoseri';

    $this->load->view('marketing/karoseri/karoseri_data', $data);
  }

  public function create($attr)
  {
    switch ($attr) {
      case 'karoseri':
        $where = [
          'kd_karoseri' => $this->input->post('kd_karoseri')
        ];
        $data = [
          'kd_karoseri' => $this->input->post('kd_karoseri'),
          'nm_karoseri' => $this->input->post('nm_karoseri'),
          'estimasi_hari' => $this->input->post('estimasi_hari'),
          'keterangan' => $this->input->post('keterangan'),
        ];

        $num = $this->crud_model->read('mst_karoseri', $where)->num_rows();
        if ($num > 0) {
          $this->session->set_flashdata('message', "Kode Karoseri sudah terdaftar");
        } else {
          $this->session->set_flashdata('message', "Data karoseri Berhasil di Simpan");
          $this->crud_model->create('mst_karoseri', $data);
        }


        redirect($this->agent->referrer());
        die();
        break;

      default:
        show_404();
        break;
    }
  }
}
