<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Konsumen extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   * 	- or -
   * 		http://example.com/index.php/welcome/index
   * 	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */

  public function __construct()
  {
    parent::__construct();
    if (!$this->ion_auth->logged_in()) {
      redirect('auth/login', 'refresh');
    }
    if ($this->session->role != 'marketing') {
      redirect('auth/logout', 'refresh');
    }
    $this->_init();
  }

  private function _init()
  {
    $this->output->set_template('marketing_layout');
  }

  public function baru()
  {
    $data = array(
      'page_header' => 'Daftar Konsumen Baru',
    );
    $this->load->view('marketing/konsumen/konsumen_form', $data);
  }

  public function form($id)
  {
    $data = array(
      'page_header' => 'Edit Konsumen',
      'data_konsumen' => $this->crud_model->read('dt_konsumen', ['id_konsumen' => $id])->row(),
    );
    $this->load->view('marketing/konsumen/konsumen_form', $data);
  }

  public function data()
  {

    //konfigurasi pagination
    $config['base_url'] = site_url('marketing/konsumen/data'); //site url
    $config['total_rows'] = $this->crud_model->read('dt_konsumen', ['id_marketing' => $this->session->id_marketing])->num_rows(); //total row
    $config['per_page'] = 10;  //show record per halaman
    $config["uri_segment"] = 4;  // uri parameter
    $choice = $config["total_rows"] / $config["per_page"];
    $config["num_links"] = floor($choice);

    // Membuat Style pagination untuk BootStrap v4
    $config['first_link']       = 'First';
    $config['last_link']        = 'Last';
    $config['next_link']        = 'Next';
    $config['prev_link']        = 'Prev';
    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
    $config['full_tag_close']   = '</ul></nav></div>';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']    = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['prev_tagl_close']  = '</span>Next</li>';
    $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
    $config['first_tagl_close'] = '</span></li>';
    $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tagl_close']  = '</span></li>';

    $this->pagination->initialize($config);
    $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

    //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
    $data['data'] = $this->crud_model->read_pagination('dt_konsumen', $config["per_page"], $data['page'], ['id_marketing' => $this->session->id_marketing]);

    $data['pagination'] = $this->pagination->create_links();
    $data['page_header'] = 'Data Konsumen';

    $this->load->view('marketing/konsumen/konsumen_data', $data);
  }

  public function create($attr)
  {
    switch ($attr) {
      case 'konsumen':
        $id_konsumen = $this->input->post('id_konsumen');
        $status = $this->input->post('status');
        $where = [
          'no_handphone' => $this->input->post('no_handphone')
        ];
        $data = [
          'jenis' => $this->input->post('jenis'),
          'id_marketing' => $this->session->id_marketing,
          'nm_lengkap' => $this->input->post('nm_lengkap'),
          'no_handphone' => $this->input->post('no_handphone'),
          'no_telp' => $this->input->post('no_telp'),
          'email' => $this->input->post('email'),
          'jenis_kelamin' => $this->input->post('jenis_kelamin'),
          'alamat' => $this->input->post('alamat'),
          'kota' => $this->input->post('kota'),
          'provinsi' => $this->input->post('provinsi'),
          'kode_pos' => $this->input->post('kode_pos'),
          'status' => $this->input->post('status'),
          'topik' => $this->input->post('topik'),
          'created_by' => $this->session->username,
          'created_at' => date('Y-m-d H:i:s')
        ];

        if ($id_konsumen > 0) {
          $this->session->set_flashdata('message', "Data Konsumen Berhasil di Perbaharui");
          $this->crud_model->update('dt_konsumen', ['id_konsumen' => $id_konsumen], $data);
          if ($status == 'prospek') {
            redirect('marketing/spk/form/' . $id_konsumen);
          } else {
            redirect($this->agent->referrer());
          }
        } else {
          $num = $this->crud_model->read('dt_konsumen', $where)->num_rows();
          if ($num > 0) {
            $this->session->set_flashdata('message', "Data Konsumen Gagal di Simpan, No Handphone sudah terdaftar");
            redirect($this->agent->referrer());
          } else {
            $this->session->set_flashdata('message', "Data Konsumen Berhasil di Simpan");
            $this->crud_model->create('dt_konsumen', $data);


            if ($status == 'prospek') {
              $last_id = $this->db->insert_id();
              redirect('marketing/spk/form/' . $last_id);
            } else {
              redirect($this->agent->referrer());
            }
          }
        }
        die();
        break;

      default:
        show_404();
        break;
    }
  }

  public function delete($attr, $id = null)
  {
    switch ($attr) {
      case 'konsumen':
        $where = [
          'id_konsumen' => $id
        ];
        $this->session->set_flashdata('message', "Data Konsumen Berhasil di Hapus");
        $this->crud_model->delete('dt_konsumen', $where);
        redirect($this->agent->referrer());
        die();
        break;

      default:
        # code...
        break;
    }
  }
}
