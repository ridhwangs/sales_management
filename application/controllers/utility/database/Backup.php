<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Backup extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   * 	- or -
   * 		http://example.com/index.php/welcome/index
   * 	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */

  public function __construct()
  {
    parent::__construct();
    if (!$this->ion_auth->logged_in()) {
      redirect('auth/login', 'refresh');
    }
    $this->_init();
  }

  private function _init()
  {
    $this->output->set_template('main_layout');
  }

  public function index()
  {
    $data = array(
      'page_header' => 'Download Database',
    );
    $this->load->view('utility/database/backup/backup_index', $data);
  }

  public function manual()
  {
    $data = array(
      'page_header' => 'Backup Database',
    );
    $this->load->view('utility/database/backup/backup_manual', $data);
  }

  public function create($attr)
  {
    switch ($attr) {
      case 'backup':

        $this->load->dbutil();

        $prefs = array(
          'format' => 'zip',
          'filename' => 'sys_ipl.sql'
        );

        $backup = &$this->dbutil->backup($prefs);

        $db_name = 'sys_ipl-' . date("YmdHis") . '.zip'; // file name
        $save  = 'backup/db/' . $db_name; // dir name backup output destination

        $this->load->helper('file');
        write_file($save, $backup);

        $this->load->helper('download');
        // force_download($db_name, $backup);
        redirect($this->agent->referrer());
        break;

      default:
        show_404();
        break;
    }
  }

  public function delete($attr, $id = null)
  {
    switch ($attr) {
      case 'backup':
        unlink('backup/db/' . $id);
        redirect($this->agent->referrer());
        break;

      default:
        # code...
        break;
    }
  }
}
