<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Mengelola extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   * 	- or -
   * 		http://example.com/index.php/welcome/index
   * 	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */

  public function __construct()
  {
    parent::__construct();
    if (!$this->ion_auth->logged_in()) {
      redirect('auth/login', 'refresh');
    }
    $this->load->helper('url');

    $this->_init();
  }

  private function _init()
  {
    $this->output->set_template('supervisor_layout');
  }

  public function dokumen()
  {
    $data = array(
      'page_header' => 'Dashboard',
    );
    $this->load->view('supervisor/dokumen/dokumen_index', $data);
  }

  public function upload()
  {
    $pathinfo = pathinfo($_FILES["berkas"]["name"]);
    $filetype = $pathinfo["extension"];
    $fileName = $this->clean($pathinfo["basename"]);

    $config['upload_path'] = './assets/dokumen';
    $config['file_name'] = $fileName;
    $config['allowed_types'] = 'txt|pdf|xls|xlsx|doc|docx|pptx|ppt|jpg|png|jpeg';
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('berkas')) {
      $this->session->set_flashdata('message', "File gagal di Import");
    } else {
      $this->session->set_flashdata('message', "File berhasil di Import");
    }
    redirect($this->agent->referrer());
  }

  public function delete($attr, $id)
  {
    switch ($attr) {
      case 'file':
        unlink('assets/dokumen/' . $id);
        redirect($this->agent->referrer());
        die();
        break;

      default:
        show_404();
        break;
    }
  }

  private function clean($string)
  {
    $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
  }
}
