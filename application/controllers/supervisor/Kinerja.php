<?php
error_reporting(0);
defined('BASEPATH') or exit('No direct script access allowed');


class Kinerja extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   * 	- or -
   * 		http://example.com/index.php/welcome/index
   * 	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */

  public function __construct()
  {
    parent::__construct();
    if (!$this->ion_auth->logged_in()) {
      redirect('auth/login', 'refresh');
    }
    $this->_init();
    $this->load->model(['kinerja_model']);
  }

  private function _init()
  {
    $this->output->set_template('supervisor_layout');
  }


  public function marketing()
  {
    //konfigurasi pagination
    $config['base_url'] = site_url('supervisor/kinerja/marketing'); //site url
    $config['total_rows'] = $this->kinerja_model->read(['dt_marketing.id_supervisor' => $this->session->id_supervisor])->num_rows(); //total row
    $config['per_page'] = 10;  //show record per halaman
    $config["uri_segment"] = 4;  // uri parameter
    $choice = $config["total_rows"] / $config["per_page"];
    $config["num_links"] = floor($choice);

    // Membuat Style pagination untuk BootStrap v4
    $config['first_link']       = 'First';
    $config['last_link']        = 'Last';
    $config['next_link']        = 'Next';
    $config['prev_link']        = 'Prev';
    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
    $config['full_tag_close']   = '</ul></nav></div>';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']    = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['prev_tagl_close']  = '</span>Next</li>';
    $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
    $config['first_tagl_close'] = '</span></li>';
    $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tagl_close']  = '</span></li>';

    $this->pagination->initialize($config);
    $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

    //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
    $data['data'] = $this->kinerja_model->read_pagination($config["per_page"], $data['page'], ['dt_marketing.id_supervisor' => $this->session->id_supervisor]);

    $data['pagination'] = $this->pagination->create_links();
    $data['page_header'] = 'Kinerja Marketing';

    $this->load->view('supervisor/kinerja/kinerja_marketing', $data);
  }

  public function create($attr)
  {
    switch ($attr) {
      case 'marketing':
        $spv_data = $this->crud_model->read('dt_supervisor', ['email' => $this->session->email])->row();
        $where = [
          'email' => $this->input->post('email'),
        ];
        $data = [
          'id_supervisor' => $spv_data->id_supervisor,
          'no_identitas' => $this->input->post('no_identitas'),
          'nm_lengkap' => $this->input->post('nm_lengkap'),
          'no_handphone' => $this->input->post('no_handphone'),
          'alamat' => $this->input->post('alamat'),
          'email' => $this->input->post('email'),
        ];

        $num = $this->crud_model->read('mst_spk', $where)->num_rows();
        if ($num > 0) {
          $this->session->set_flashdata('message', "Email sudah terdaftar");
        } else {
          $this->session->set_flashdata('message', "Data Marketing Berhasil di Simpan");
          $this->crud_model->create('mst_spk', $data);
        }
        redirect($this->agent->referrer());
        die();
        break;

      default:
        show_404();
        break;
    }
  }

  public function update($attr, $id_spk = null)
  {
    switch ($attr) {
      case 'approve':
        $where = [
          'id_spk' => $id_spk
        ];
        $data = [
          'status' => 'approve'
        ];
        $this->session->set_flashdata('message', "Data berhasil di Approve");
        $this->crud_model->update('mst_spk', $where, $data);
        redirect($this->agent->referrer());
        die();
        break;
      case 'reject':
        $where = [
          'id_spk' => $id_spk
        ];
        $data = [
          'status' => 'reject'
        ];
        $this->session->set_flashdata('message', "Data berhasil di Reject");
        $this->crud_model->update('mst_spk', $where, $data);
        redirect($this->agent->referrer());
        die();
        break;
      default:
        show_404();
        break;
    }
  }
}
